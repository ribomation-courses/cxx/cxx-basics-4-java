package ribomation;

public class FancyApp {
    public static void main(String[] args) {
        int n = args.length == 0 ? 35 : Integer.parseInt(args[0]);
        System.out.printf("fib(%d) ...%n", n);
        long start = System.nanoTime();
        long result = new FancyCxx().fibonacci(n);
        long end = System.nanoTime();
        System.out.printf("fib(%d) = %d (elapsed %.4f s)%n",
                n, result, (end - start) * 1E-9);
    }
}
