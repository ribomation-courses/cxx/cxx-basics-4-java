Testing Java/C++ JNI, for Windows
====

Prerequisites
----

* Install [Build Tools for Visual Studio 2017](https://www.visualstudio.com/downloads/).
  Scroll down to the end, it's the third item inside "_Other Tools and Frameworks_".

* Install [JetBrains CLion for Windows](https://www.jetbrains.com/clion/download/#section=windows).
  Configure the default toolchain for Visual Studio. Just navigate to the install dir and CLion
  will pick up the rest.

* Install [Java 8 JDK](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)

* Install [JetBrains IntelliJ IDEA, for Windows](https://www.jetbrains.com/idea/download/#section=windows)

Steps
----

1. Create a _Java_ project in the `./java/` sub-dir, using _Intellij_
1. Compile the Java sources
1. Run the `generator.bat` script, which uses `javah` to create a JNI C++ header in the cxx directory
1. Create a _C++ Library_ project in the `./cxx/` sub-dir, using CLion
1. Adjust the path of the include dirs in `./cxx/CMakeLists.txt`, so they are correct
1. Build the C++ library. The DLL file `tstjni.dll` can then be found in the
   `./cxx/cmake-build-debug/` dir
1. Run the Java program using the `run.bat` script. It uses the
  `-Djava.library.path=` option to point to the directory of the DLL file.
  For example:

    run.bat 42

Links
----
* [Wikipedia - Java Native Interface](https://en.wikipedia.org/wiki/Java_Native_Interface)
* [IBM Developer Works - Java programming with JNI](https://www.ibm.com/developerworks/java/tutorials/j-jni/j-jni.html)
* [Java Programming Tutorial - Java Native Interface (JNI)](https://www.ntu.edu.sg/home/ehchua/programming/java/JavaNativeInterface.html)
* [Oracle - Java Native Interface Specification](https://docs.oracle.com/javase/8/docs/technotes/guides/jni/spec/jniTOC.html)
