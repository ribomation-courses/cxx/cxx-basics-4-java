#include "ribomation_FancyCxx.hxx"

static long fib(int n) {
    return n <= 2 ? 1 : fib(n - 2) + fib(n - 1);
}


JNIEXPORT jlong JNICALL
Java_ribomation_FancyCxx_fibonacci(JNIEnv* env, jobject obj, jint arg) {
    return fib(arg);
}
