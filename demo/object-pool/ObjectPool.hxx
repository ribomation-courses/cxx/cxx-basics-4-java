#pragma once

#include <array>
#include <bitset>
#include <stdexcept>
#include <cstring>


namespace ribomation {
    namespace memory {
        using namespace std;
        using byte = unsigned char;

        template<typename Type, unsigned SIZE>
        class Pool {
            constexpr static size_t OBJECT_SIZE  = sizeof(Type);
            constexpr static size_t STORAGE_SIZE = SIZE * OBJECT_SIZE;

            byte         storage[STORAGE_SIZE];
            bitset<SIZE> usage;

            unsigned nextAvailable() const {
                unsigned idx = 0;
                while (idx < usage.size() && usage.test(idx)) {
                    ++idx;
                }
                return idx;
            }

            size_t indexOf(const Type* ptr) const {
                return (reinterpret_cast<size_t>(ptr) - reinterpret_cast<size_t>(storage)) / OBJECT_SIZE;
            }

        public:
            Pool() {
                ::memset(storage, '\0', STORAGE_SIZE);
                usage.reset();
            }

            Type* alloc() {
                if (full()) {
                    throw overflow_error("pool full");
                }

                const auto idx = nextAvailable();
                usage.set(idx);
                return reinterpret_cast<Type*>(storage + idx * OBJECT_SIZE);
            }

            void dispose(Type* ptr) {
                const byte* address = reinterpret_cast<byte*>(ptr);
                if (address < storage || (storage + STORAGE_SIZE) < address) {
                    throw overflow_error("block not belonging to this pool");
                }

                usage.reset(indexOf(ptr));
                ::memset(ptr, '\0', OBJECT_SIZE);
            }

            size_t capacity() const {
                return SIZE;
            }

            size_t size() const {
                return usage.count();
            }

            size_t free() const {
                return capacity() - size();
            }

            bool full() const {
                return free() == 0;
            }

            static size_t storageRequirement() {
                return sizeof(Pool<Type, SIZE>);
            }


            ~Pool() = default;
            Pool(const Pool<Type, SIZE>&) = delete;
            Pool<Type, SIZE>& operator =(const Pool<Type, SIZE>&)= delete;
        };

    }
}
