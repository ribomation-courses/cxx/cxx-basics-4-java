#include <iostream>

using namespace std;

template<typename T>
class Ptr {
    T* addr;
public:
    explicit Ptr(T* addr) : addr{addr} {}
    ~Ptr() { delete addr; }

    T&  operator *()  const {return *addr;}
    T*  operator ->() const {return addr;}

    Ptr()              = delete;
    Ptr(const Ptr<T>&) = delete;

    Ptr<T>&     operator=(const Ptr<T>&) = delete;
    const void* operator&() const        = delete;
    void*       operator new(size_t)     = delete;
};

void using_ints() {
    cout << "--- using ints ---\n";
    Ptr<int> p{new int{42}};
    cout << "*p = " << *p << endl;
    *p *= 10;
    cout << "*p = " << *p << endl;
}

void using_accounts() {
    cout << "--- using accounts ---\n";
    struct Account {
      private: int balance;
      public:  Account(int b) : balance{b} {}
      public:  int  getBalance() const {return balance;}
      public:  void updateBalance(int b) {balance += b;}
    };

    Ptr<Account> p{new Account{1500}};
    cout << "p->getBalance()   = " << p->getBalance() << endl;
    cout << "(*p).getBalance() = " << (*p).getBalance() << endl;
    p->updateBalance(-750);
    cout << "p->getBalance()   = " << p->getBalance() << endl;
}


int main() {
    using_ints();
    using_accounts();

    //void* x = &p;
    //error: use of deleted function ‘const void* Ptr::operator&() const’

    //void* y = new Ptr{new int{42}};
    //error: use of deleted function ‘static void* Ptr::operator new(size_t)’

    return 0;
}
