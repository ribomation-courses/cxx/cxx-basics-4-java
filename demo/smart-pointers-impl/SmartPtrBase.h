#ifndef __SMARTPTRBASE__
#define __SMARTPTRBASE__

class NullPointerException {};

template<typename TargetType>
class SmartPtrBase {
	TargetType*		thePointer;

	protected:
	TargetType*  ptr() {
		if (!thePointer) throw NullPointerException();
		return thePointer;
	}
	void ptr(TargetType* p) { thePointer = p; }
	
	void clear() { thePointer = 0; }
	void dispose() { delete thePointer; }
	
	protected: const void*  operator &() const {return this;}
	private:   void*        operator new(size_t) {return 0;}	
	
	public:    TargetType*  operator ->() {return this->ptr();}	
	public:    TargetType&  operator  *() {return *( this->ptr() );}
};

#endif
