#ifndef __PERSON__
#define __PERSON__
#include <string>
#include "trace.h"
#include "InstanceCount.h"
using namespace std;

class Person : public InstanceCount {
	Trace      trace;
	string    _name;
	int       _age;
	Person&  operator =(const Person&) {return *this;}
	
	public:
	Person(string n, int a=30) : trace("Person("+n+")", this), _name(n), _age(a) {}	
	Person(const Person& p) : trace("PERSON("+p.name()+")", this), _name(p.name()), _age(p.age()) {}
	
	string name() const {return _name;}
	int    age()  const {return _age;}
	
	friend ostream&  operator <<(ostream& out, Person& p) {
		return out << "Person[name=" << p.name() << ", age=" << p.age() << "]";
	}
};

#endif
