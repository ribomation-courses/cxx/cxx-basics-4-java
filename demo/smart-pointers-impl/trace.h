#ifndef __TRACE__
#define __TRACE__
#include <string>
#include <sstream>
using namespace std;

class Trace {
  string   _name;
  void*    _address;
  
  friend ostream&  operator <<(ostream& os, const Trace& t) {
    os << "[" << t.name();
    if (t.address()) os << "@" << t.address();
    return  os << "]";
  }
  
  
public:
  Trace(const string n="BLOCK", void* addr=0, const char* msg=0) 
    : _name(n), _address(addr) 
  {
    *this << " ENTER ";
	if (msg) *this << msg;
  }  
  
  Trace(const Trace& t) : _name(t._name), _address(this) {
	*this << " ENTER ";
  }
  
  ~Trace() {
    *this << " EXIT";
  }
  
  template<typename ReturnType>
  ReturnType  RETURN(ReturnType value) {
    ostringstream buf;
	buf << " RETURN(" << value << ")";
	*this << buf.str();
	return value;
  }
  
  string name() const {return _name;}
  const void*  address() const {return _address;}
  
  Trace& operator <<(const char* msg) {
    cerr << *this << " " << msg << endl;
	return *this;
  }
  
  Trace& operator <<(const string& msg) {
    cerr << *this << " " << msg << endl;
	return *this;
  }  
  
};



#endif
