//compile: g++ -std=c++11 -fmax-errors=1 -Wall SharedPtrTest.cpp -o shared-ptr
//run    : ./shared-ptr

#include <iostream>
#include <memory>
using namespace std;

struct Account {
    int     balance = 0;

    Account() {
        cout<<"Account() @ "<<this<<endl;
    }
    Account(const Account& that) : balance(that.balance) { 
        cout<<"Account(const Account&) @ "<<this<<endl;
    }
    ~Account() {
        cout<<"~Account() @ "<<this<<endl;
    }
    Account&  operator=(const Account& that) {
        if (this != &that) {this->balance = that.balance;}
        return *this;
    }
    
    int  update(int amount) {
        return (balance += amount);
    }
};

ostream&  operator<<(ostream& os, const Account& a) {
    return os << "Account[" << a.balance << " kr] @ " << &a;
}

shared_ptr<Account>  update(shared_ptr<Account> a) {
    a->update(100);
    return a;
}

int main() {
  cout << "[main] enter" << endl;
  {
    shared_ptr<Account>     a1( new Account );
    cout << "a1 = " << *a1 << endl;
    
    cout << "[main] ------" << endl;
    shared_ptr<Account>     a2 = update(a1);
    cout << "a1 = " << *a1 << endl;
    cout << "a2 = " << *a2 << endl;
    
    cout << "[main] ------" << endl;
    shared_ptr<Account>     a3;
    a3 = a2;
    cout << "a1 = " << *a1 << endl;
    cout << "a2 = " << *a2 << endl;
    cout << "a3 = " << *a3 << endl;
    cout << "a3.cnt = " << a3.use_count() << endl;
  }
  cout << "[main] exit" << endl;
  
    return 0;
}

