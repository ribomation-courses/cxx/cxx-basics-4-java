#ifndef __UNIQUEPTR__
#define __UNIQUEPTR__
#include <iostream>
#include "SmartPtrBase.h"

template<typename TargetType>
class UniquePtr : public SmartPtrBase<TargetType> {	
	void  assign(const UniquePtr<TargetType>& obj) {
		UniquePtr<TargetType>&  that = const_cast< UniquePtr<TargetType>& >(obj);
		this->ptr( that.ptr() );
		that.clear();
	}
	
	void  assign(TargetType* p) { 
		this->ptr(p); 
	}

	public:
	UniquePtr(TargetType* p) { assign(p); }	
	UniquePtr(const UniquePtr<TargetType>& p) { assign(p); }
	~UniquePtr() { this->dispose(); }
	UniquePtr<TargetType>&  operator =(const UniquePtr<TargetType>& ptr) {
		if (this != &ptr) { this->dispose(); assign(ptr); }
		return *this;
	}
};

#endif
