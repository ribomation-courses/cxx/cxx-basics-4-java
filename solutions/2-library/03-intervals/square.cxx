#include <iostream>
#include <iterator>
using namespace std;

int main() {
    istream_iterator<int> in{cin};
    istream_iterator<int> eof{};
    ostream_iterator<int> out{cout, " "};

    for (; in != eof; ++in, ++out) {
        auto n = *in;
        *out = n * n;
    }
    cout << endl;

    return 0;
}
