#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <stdexcept>
#include <cctype>

using namespace std;
using namespace std::literals;

int main(int argc, char** argv) {
    auto const filename = "../words.cxx"s;
    auto       file     = ifstream{filename};
    if (!file) throw invalid_argument{"cannot open "s + filename};

    auto      words = vector<string>{};
    for (auto w     = ""s; file >> w;) {
        words.push_back(w);
    }

    cout << "(1) print: ";
    for (auto const& w : words) cout << w << " ";
    cout << endl;

    cout << "(2) count: ";
    auto cnt = 0U;
    for (auto const& w : words) cnt += w.size();
    cout << cnt << " chars" << endl;

    cout << "(3) insert: ";
    words.insert(words.begin(), "TJABBA HABBA"s);
    for (auto const& w : words) cout << w << " ";
    cout << endl;

    cout << "(4) strip: ";
    for (auto& w : words) {
        auto      s = ""s;
        for (auto ch:w) if (isalpha(ch)) s += ch;
        w = s;
    }
    for (auto const& w : words) cout << w << " ";
    cout << endl;

    cout << "(5) pop: ";
    while (!words.empty()) {
        auto w = words.back();
        words.pop_back();
        if (!w.empty()) cout << w << "/";
    }
    cout << endl;

    return 0;
}
