cmake_minimum_required(VERSION 3.14)
project(04_algorithms)

set(CMAKE_CXX_STANDARD 17)

add_executable(word-cloud word-cloud.cxx)

