#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <stdexcept>
#include <algorithm>
#include <cctype>
#include <string>
#include <unordered_map>
#include <vector>
#include <utility>
#include <random>

using namespace std;
using namespace std::literals;
using Freqs = unordered_map<string, unsigned int>;
using WordFreq = pair<string, unsigned>;

auto load(istream& infile, unsigned minWordSize) -> Freqs;
void store(string filename, const vector<string>& tags);
auto randColor() -> string;
auto sorted(Freqs& frequencies, unsigned maxNumWords) -> vector<WordFreq>;
auto mkTags(vector<WordFreq> words, float minFontSize, float maxFontSize) -> vector<string>;


int main(int argc, char** argv) {
    const auto minWordSize = 4U;
    const auto maxNumWords = 100U;
    const auto minFontSize = 15.F;
    const auto maxFontSize = 150.F;
    const auto outFilename = "word-cloud.html"s;
    auto       filename    = (argc > 1) ? argv[1] : "../files/musketeers.txt"s;

    auto infile = ifstream{filename};
    if (!infile) throw invalid_argument{"cannot open "s + filename};

    auto frequencies = load(infile, minWordSize);
    auto words       = sorted(frequencies, maxNumWords);
    auto tags        = mkTags(words, minFontSize, maxFontSize);
    store(outFilename, tags);
    cout << "written " << outFilename << endl;

    return 0;
}

auto load(istream& infile, unsigned minWordSize) -> Freqs {
    auto      frequencies = Freqs{};
    for (auto line        = ""s; getline(infile, line);) {
        transform(begin(line), end(line), begin(line), [](auto ch) {
            return isalpha(ch) ? tolower(ch) : ' ';
        });

        auto      buf  = istringstream{line};
        for (auto word = ""s; buf >> word;) {
            if (word.size() >= minWordSize) {
                ++frequencies[word];
            }
        }
    }
    return frequencies;
}

auto sorted(Freqs& frequencies, unsigned maxNumWords) -> vector<WordFreq> {
    auto sortable = vector<WordFreq>{begin(frequencies), end(frequencies)};

    sort(begin(sortable), end(sortable), [](WordFreq lhs, WordFreq rhs) {
        return lhs.second > rhs.second;
    });

    auto words = vector<WordFreq>{};
    copy_n(begin(sortable), maxNumWords, back_inserter(words));

    return words;
}

auto mkTags(vector<WordFreq> words, float minFontSize, float maxFontSize) -> vector<string> {
    const auto maxFreq = words.front().second;
    const auto minFreq = words.back().second;
    auto       scale   = (maxFontSize - minFontSize) / (maxFreq - minFreq);

    auto tags = vector<string>{};
    transform(begin(words), end(words), back_inserter(tags), [=](WordFreq& wf) {
        return "<span style='"s
               + "font-size: "s + to_string(static_cast<unsigned>(wf.second * scale + minFontSize)) + "px;"s
               + "color: #"s + randColor() + ";"s
               + "'>"s
               + wf.first + "</span>"s;
    });

    random_device r;
    shuffle(begin(tags), end(tags), r);

    return tags;
}

string randColor() {
    random_device                      r;
    uniform_int_distribution<unsigned> color{0, 255};

    ostringstream buf;
    buf << hex << color(r) << color(r) << color(r);
    return buf.str();
}

void store(string filename, const vector<string>& tags) {
    auto outfile = ofstream{filename};
    if (!outfile) throw invalid_argument{"cannot open "s + filename};

    outfile << R"(<html>
    <head>
        <title>Word Cloud</title>
        <style>
            body {font-family: sans-serif;}
            h1   {text-align: center; color: orange;}
        </style>
    </head>
    <body> <h1>Word Cloud</h1>
    )";
    for (auto& tag : tags) {
        outfile << tag << endl;
    }
    outfile << "</body></html>\n";
}

