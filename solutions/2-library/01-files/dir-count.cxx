#include <iostream>
#include <string>
#include <unordered_set>
#include <filesystem>
#include "each.hxx"
#include "count.hxx"

using namespace std;
using namespace std::literals;
using namespace ribomation::util;
using namespace ribomation::io;
namespace fs = std::filesystem;

bool isTextFile(fs::path const& f) {
    static unordered_set<string> extensions = {
            ".txt"s, ".hxx"s, ".cxx"s
    };
    return extensions.count(f.extension()) == 1;
}

int main(int argc, char** argv) {
    auto dir = fs::current_path() / ".."s;

    Count TOTAL{"TOTAL"};
    for (auto const& e : fs::recursive_directory_iterator(dir)) {
        if (isTextFile(e.path())) {
            auto p = fs::relative(e.path(), dir);
            Count FILE{p.string()};
            try {
                each(e.path().string(), [&](auto line) {
                    FILE += line;
                    TOTAL += line;
                });
            } catch (const invalid_argument& x) {
                cout << "** " << x.what() << endl;
            }
            cout << FILE << endl;
        }
    }
    cout << TOTAL << endl;

    return 0;
}
