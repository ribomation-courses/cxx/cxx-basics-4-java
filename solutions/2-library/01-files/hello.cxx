#include <iostream>
#include "numbers.hxx"
using namespace std;
using namespace std::literals;


int main(int argc, char** argv) {
    cout << "Tjabba from C++\n";

    auto const n = 10;
    cout << "sum(1.." << n << ") = " << sum(n) << endl;
    cout << "prod(1.." << n << ") = " << prod(n) << endl;
    return 0;
}
