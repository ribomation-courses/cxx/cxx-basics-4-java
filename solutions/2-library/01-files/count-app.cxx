#include <iostream>
#include <string>
#include <algorithm>
#include "each.hxx"
#include "count.hxx"

using namespace std;
using namespace ribomation::io;
using namespace ribomation::util;


int main(int numArgs, char* args[]) {
    if (numArgs <= 1) {
        Count STDIN{"STDIN"};
        each(cin, [&](auto line) { STDIN += line; });
        cout << STDIN << endl;
        return 0;
    }

    Count TOTAL{"TOTAL"};
    for_each(args + 1, args + numArgs, [&](const string& filename) {
        Count FILE{filename};
        each(filename, [&](auto line) {
            FILE += line;
            TOTAL += line;
        });
        cout << FILE << endl;
    });
    if (numArgs > 2) cout << TOTAL << endl;

    return 0;
}
