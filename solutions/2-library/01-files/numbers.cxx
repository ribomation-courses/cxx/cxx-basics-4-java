#include "numbers.hxx"

long sum(int n) {
    if (n <= 0) return 0;
    return n * (n + 1) / 2;
}



long prod(int n) {
    return n <= 1 ? 1 : n * prod(n - 1);
}

