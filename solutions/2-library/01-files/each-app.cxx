#include <iostream>
#include "each.hxx"

using namespace std;
using namespace std::literals;
using namespace ribomation::io;

int main(int argc, char** argv) {
    auto const filename = "../each-app.cxx"s;

    auto lineno = 1U;
    each(filename, [&](auto line){
        cout << lineno++ << ") " << line << endl;
    });

    return 0;
}
