#include <iostream>
#include <string>
using namespace std;

void swap(int& a, int& b) {
    int t = a;
    a = b;
    b = t;
}

void swap2(int* a, int* b) {
    int t = *a;
    *a = *b;
    *b = t;
}

int main(int argc, char** argv) {
    int a = (argc >= 2) ? stoi(argv[1]) : 10;
    int b = (argc >= 3) ? stoi(argv[2]) : 20;

    cout << "[before] <" << a << ", " << b << ">" << endl;
    swap(a, b);
    cout << "[after]  <" << a << ", " << b << ">" << endl;

    swap2(&a, &b);
    cout << "[after2] <" << a << ", " << b << ">" << endl;

    return 0;
}
