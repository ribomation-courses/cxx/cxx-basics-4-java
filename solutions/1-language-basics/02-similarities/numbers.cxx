#include <iostream>
using namespace std;

int main() {
    unsigned count = 0;
    int      sum   = 0;
    int      min   = +9999;
    int      max   = -9999;

    do {
        cout << "Give number: ";
        int num;
        cin >> num;
        if (num == 0) break;

        ++count;    // count = count + 1
        sum += num; // sum = sum + num
        min = (num < min) ? num : min; // if (num < min) min = num;
        max = (num > max) ? num : max; // if (num > max) max = num;
    } while (true);

    cout << "Count  : " << count << endl;
    cout << "Sum    : " << sum << endl;
    cout << "Average: " << (static_cast<double>(sum) / count) << endl;
    cout << "Min/Max: " << min << " / " << max << endl;

    return 0;
}
