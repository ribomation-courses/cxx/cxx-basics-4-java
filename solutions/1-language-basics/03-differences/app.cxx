#include <iostream>
#include <string>
#include <chrono>
#include "fibonacci.hxx"

using namespace std;
using namespace std::chrono;
using namespace ribomation;

int main(int numArgs, char* args[]) {
    int N = 42;
    if (numArgs > 1) { N = stoi(args[1]); }
    {
        ribomation::useRecursion = true;
        cout << "fibonacci(" << N << ") = ..." << endl;
        auto start   = steady_clock::now();
        auto result  = fibonacci(N);
        auto elapsed = steady_clock::now() - start;
        cout << "fibonacci(" << N << ") = " << result
             << ". Elapsed " << duration_cast<microseconds>(elapsed).count() << "us" << endl;
    }
    {
        ribomation::useRecursion = false;
        cout << "fibonacci(" << N << ") = ..." << endl;
        auto start               = steady_clock::now();
        auto result              = fibonacci(N);
        auto elapsed             = steady_clock::now() - start;
        cout << "fibonacci(" << N << ") = " << result
             << ". Elapsed " << duration_cast<microseconds>(elapsed).count() << "us" << endl;
    }
    return 0;
}
