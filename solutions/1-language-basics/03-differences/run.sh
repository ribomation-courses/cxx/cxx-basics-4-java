#!/usr/bin/env bash
set -eux

CXX_FLAGS="-std=c++17 -Wall -Wextra -Werror -Wfatal-errors"

mkdir bld
cd bld
g++ ${CXX_FLAGS} -c ../fibonacci.cxx
g++ ${CXX_FLAGS} -c ../app.cxx
g++ fibonacci.o app.o -o fib
ls -lhF
./fib
