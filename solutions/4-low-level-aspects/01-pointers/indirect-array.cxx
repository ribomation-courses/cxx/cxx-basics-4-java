#include <iostream>

using namespace std;
using namespace std::literals;

int main(int argc, char** argv) {
    int arr[] = {10,20,30,40,50};
    auto const N = sizeof(arr) / sizeof(arr[0]);

    cout << "arr: ";
    for (auto n : arr) cout << n << " ";
    cout << endl;

    int* ptr = &arr[0];
    cout << "*ptr: " << *ptr << ", " << *(&arr[0]) << ", " << *arr << endl;

    cout << "ptr: ";
    for (int* p = &arr[0]; p != &arr[N]; ++p) cout << *p << " ";
    cout << endl;

    for (int* p = &arr[0]; p != &arr[N]; ++p) *p *= 10;

    cout << "ptr: ";
    for (int* p = &arr[0]; p != &arr[N]; ++p) cout << *p << " ";
    cout << endl;

    return 0;
}