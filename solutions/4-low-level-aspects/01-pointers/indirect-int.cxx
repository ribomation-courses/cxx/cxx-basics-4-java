#include <iostream>

using namespace std;
using namespace std::literals;

int main(int argc, char** argv) {
    int number = 42;
    cout << "number: " << number << endl;

    int* ptr = &number;
    cout << "ptr: " << *ptr << " @ " << ptr << endl;

    *ptr *= 10;
    cout << "ptr: " << *ptr << " @ " << ptr << endl;
    cout << "number: " << number << endl;

    return 0;
}
