cmake_minimum_required(VERSION 3.14)
project(01_pointers)

set(CMAKE_CXX_STANDARD 17)

add_executable(indirect-int indirect-int.cxx)
add_executable(indirect-array indirect-array.cxx)

