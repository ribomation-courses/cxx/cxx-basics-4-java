#include <iostream>
#include <cstdint>

using namespace std;
using namespace std::literals;

struct Package {
    uint32_t payload;   //+4

    uint16_t chksum:4;  //+2
    uint16_t addr:12;

    uint8_t :8;         //+1

    uint8_t opt:2;      //+1
    uint8_t mode:2;
    uint8_t type:4;
};

int main() {
    cout << "sizeof(Package): " << sizeof(Package) << " bytes" << endl;

    uint64_t data = 0b1001'10'11'00000000'000000101010'0111'00000000'01111001'01100101'01001000;
    //                9    2  3  padding  42           7    \0       y        e        H

    Package* p = reinterpret_cast<Package*>(&data);
    cout << "p.type   : " << static_cast<unsigned short>(p->type) << endl;
    cout << "p.mode   : " << static_cast<unsigned short>(p->mode) << endl;
    cout << "p.opt    : " << static_cast<unsigned short>(p->opt) << endl;
    cout << "p.addr   : " << static_cast<unsigned short>(p->addr) << endl;
    cout << "p.chksum : " << static_cast<unsigned short>(p->chksum) << endl;
    cout << "p.payload: '" << reinterpret_cast<char*>( &(p->payload)) << "'" << endl;

    return 0;
}
