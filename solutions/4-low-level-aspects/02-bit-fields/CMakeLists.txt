cmake_minimum_required(VERSION 3.14)
project(02_bit_fields)

set(CMAKE_CXX_STANDARD 17)

add_executable(bit-fields bit-fields.cxx)
