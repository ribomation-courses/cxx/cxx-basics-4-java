#include <iostream>
#include <sstream>
#include "vector-3d.hxx"

using namespace std;
using namespace std::literals;
using namespace ribomation;

int main() {
    Vector3d<int> v{1, 2, 3};
    cout << "v: " << v << endl;

    cout << "10 * v: " << (10 * v) << endl;
    cout << "v * 10: " << (v * 10) << endl;
    cout << "v + v: " << (v + v) << endl;
    cout << "v - v: " << (v - v) << endl;
    cout << "v * v: " << (v * v) << endl;

    istringstream buf{"10 20 30"s};
    buf >> v;
    cout << "v: " << v << endl;

    return 0;
}

