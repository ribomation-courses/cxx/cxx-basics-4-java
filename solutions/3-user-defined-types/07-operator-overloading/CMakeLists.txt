cmake_minimum_required(VERSION 3.14)
project(08_operator_overloading)

set(CMAKE_CXX_STANDARD 17)

add_executable(vector
        vector-3d.hxx
        vector-app.cxx
)

