#pragma once

#include <iosfwd>

namespace ribomation {
    using namespace std;

    template<typename T=double>
    class Vector3d {
        T x, y, z;
    public:
        Vector3d(T x, T y, T z) : x{x}, y{y}, z{z} {}
        T getX() const { return x; }
        T getY() const { return y; }
        T getZ() const { return z; }
    };

    template<typename T>
    ostream& operator <<(ostream& os, const Vector3d<T>& v) {
        return os << "[" << v.getX() << ", " << v.getY() << ", " << v.getZ() << "]";
    }

    template<typename T>
    istream& operator >>(istream& is, Vector3d<T>& v) {
        T x, y, z;
        is >> x >> y >> z;
        v = Vector3d<T>{x, y, z};
        return is;
    }

    template<typename T>
    Vector3d<T> operator *(int factor, const Vector3d<T>& v) {
        return {factor * v.getX(), factor * v.getY(), factor * v.getZ()};
    }

    template<typename T>
    Vector3d<T> operator *(const Vector3d<T>& v, int factor) {
        return factor * v;
    }

    template<typename T>
    Vector3d<T> operator +(const Vector3d<T>& lhs, const Vector3d<T>& rhs) {
        return {
                lhs.getX() + rhs.getX(),
                lhs.getY() + rhs.getY(),
                lhs.getZ() + rhs.getZ()
        };
    }

    template<typename T>
    Vector3d<T> operator -(const Vector3d<T>& lhs, const Vector3d<T>& rhs) {
        return {
                lhs.getX() - rhs.getX(),
                lhs.getY() - rhs.getY(),
                lhs.getZ() - rhs.getZ()
        };
    }

    template<typename T>
    T operator *(const Vector3d<T>& lhs, const Vector3d<T>& rhs) {
        return lhs.getX() * rhs.getX()
               + lhs.getY() * rhs.getY()
               + lhs.getZ() * rhs.getZ();
    }

}
