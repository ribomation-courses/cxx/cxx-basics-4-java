cmake_minimum_required(VERSION 3.9)
project(7_inheritance)

set(CMAKE_CXX_STANDARD 17)
add_compile_options(-Wall -Wextra  -Wfatal-errors)

add_executable(shapes
        Shape.hxx
        Rect.hxx
        Square.hxx
        Circle.hxx
        Triangle.hxx
        shapes.cxx
        )
