#include <iostream>
#include <stdexcept>
#include "trace.hxx"

using namespace std;
using namespace std::literals;
using namespace ribomation;

int Trace::indent = 0;

class Person {
    Trace  t;
    string name;
public:
    Person(string n) : t{"Person:"s + n, reinterpret_cast<unsigned long>(this)}, name{n} {}
};

long factorial(int n) {
    Trace t{"factorial("s + to_string(n) + ")"s};
    Person p{"Nisse"};

    if (n <= 1) return t.RETURN(1);
    if (n == 5) {
        throw runtime_error{"bazinga"s};
    }
    return t.RETURN(n * factorial(n - 1));
}

int main() {
    {
        auto result = factorial(4);
        cout << "4! = " << result << endl;
    }
    cout << "-----\n";
    try {
        auto result = factorial(7);
        cout << "7! = " << result << endl;
    } catch (exception& x) {
        cout << "Oops: " << x.what() << endl;
    }
    return 0;
}
