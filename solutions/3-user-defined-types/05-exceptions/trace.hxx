#pragma once

#include <iostream>
#include <string>

namespace ribomation {
    using namespace std;
    using namespace std::literals;

    class Trace {
        const string  name;
        unsigned long addr;
        bool          hasReturned = false;
        static int    indent;
        constexpr static unsigned TAB = 4;

    public:
        Trace(string n, unsigned long a = 0) : name{n}, addr{a} {
            cout << "** " << string(indent, ' ') << name << " enter"
                 << (addr ? " @ "s + to_string(addr) : ""s) << endl;
            if (addr == 0) indent += TAB;
        }

        ~Trace() {
            if (hasReturned) return;
            if (addr == 0) indent -= TAB;

            cout << "** " << string(indent, ' ') << name << " leave"
                 << (addr ? " @ "s + to_string(addr) : ""s) << endl;
        }

        template<typename T>
        T RETURN(T x) {
            indent -= TAB;
            cout << "** " << string(indent, ' ') << name << " RETURN " << x << endl;
            hasReturned = true;
            return x;
        }
    };
}
