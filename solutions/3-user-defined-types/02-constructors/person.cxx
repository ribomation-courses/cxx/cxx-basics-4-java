#include <iostream>
#include <sstream>
#include <utility>
#include "person.hxx"

namespace ribomation {
    using namespace std;
    using namespace std::literals;

    Person::Person() : name{""s}, age{0} {
        cout << "Person{} @ " << this << endl;
    }

    Person::Person(string name) : name(move(name)), age(0) {
        cout << "Person{" << this->name << "} @ " << this << endl;
    }

    Person::Person(string name, unsigned int age)
            : name(move(name)), age(age) {
        cout << "Person{" << this->name << ", " << this->age << "} @ " << this << endl;
    }

    Person::Person(const Person& that): name(that.name), age(that.age) {
        cout << "Person{& " << that.toString() << "} @ " << this << endl;
    }

    Person::~Person() {
        cout << "~Person(" << name << ") @ " << this << endl;
    }

    void Person::setName(const string& name) {
        Person::name = name;
        //this->name = name;  java-style
    }

    void Person::setAge(unsigned int age) {
        Person::age = age;
    }

    string Person::toString() const {
        ostringstream buf{};
        buf << "Person{" << name << ", " << age << "} @ " << this;
        return buf.str();
    }


}
