#include <iostream>
#include "person.hxx"
using namespace std;
using namespace std::literals;
using namespace ribomation;

Person func(Person p) {
    cout << "[func] enter\n";
    cout << "[func] p: " << p.toString() << endl;
    cout << "[func] exit\n";
    return p;
}

int main() {
    cout << "[main] enter\n";
    Person l{"Locke", 30};
    cout << "l: " << l.toString() << endl;
    {
        cout << "[main] invoking func()\n";
        Person s = func("Nisse"s);
        cout << "s: " << s.toString() << endl;
        cout << "[main] leaving block\n";
    }
    {
        string   n = l;
        unsigned a = l;
        cout << "[main] type-conversion: " << n << ", " << a << endl;
    }
    {
        Person p;
        cout << "p: " << p.toString() << endl;
    }
    cout << "[main] exit\n";
    return 0;
}
