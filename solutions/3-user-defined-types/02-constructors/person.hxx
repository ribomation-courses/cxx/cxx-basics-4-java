#pragma  once

#include <string>

namespace ribomation {
    using namespace std;

    class Person {
        string   name;
        unsigned age;
    public:
        Person();
        Person(string name);
        Person(string name, unsigned int age);
        Person(const Person&);
        ~Person();

        string getName() const { return name; }
        unsigned getAge() const { return age; }

        void setName(const string& name);
        void setAge(unsigned int age);

        string toString() const;

        operator string() const {
            return name;
        }

        operator unsigned() const {
            return age;
        }
    };

}

