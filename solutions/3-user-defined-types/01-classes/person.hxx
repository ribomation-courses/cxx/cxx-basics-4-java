#pragma  once

#include <string>

namespace ribomation {
    using namespace std;

    class Person {
        string   name;
        unsigned age;
    public:
        Person(string name, unsigned int age);
        ~Person();

        string getName() const { return name; }
        unsigned getAge() const { return age; }

        void setName(const string& name);
        void setAge(unsigned int age);

        string toString() const;
    };

}

