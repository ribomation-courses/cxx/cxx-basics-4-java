cmake_minimum_required(VERSION 3.14)
project(01_classes)

set(CMAKE_CXX_STANDARD 17)

add_executable(persons
        person.hxx
        person.cxx
        person-app.cxx
        )
