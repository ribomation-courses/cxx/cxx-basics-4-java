#include <iostream>
#include "person.hxx"

using namespace std;
using namespace std::literals;
using namespace ribomation;

Person g{"Globbe", 20};

int main() {
    cout << "[main] enter\n";
    Person l{"Locke", 30};
    Person* h = new Person{"Heppe", 40};

    cout << "g: " << g.toString() << endl;
    cout << "l: " << l.toString() << endl;
    cout << "h: " << h->toString() << endl;

    {
        Person s{"Stacke", 50};
        cout << "s: " << s.toString() << endl;
    }

    delete h;
    cout << "[main] exit\n";
    return 0;
}
