#include <iostream>
#include <string>

using namespace std;
using namespace std::literals;

class Thing {
    mutable string name;
public:
    Thing(string s) : name{s} {}
    string getName() const { return name; }
    void setName(string s) const { name = s; }
};

int main() {
    Thing t{"Nisse"};
    cout << "t.name: " << t.getName() << endl;
    t.setName("Berra");
    cout << "t.name: " << t.getName() << endl;
    return 0;
}

