#include <iostream>
#include <string>
#include <utility>
#include <vector>
using namespace std;
using namespace std::literals;

template<typename T = int, unsigned N = 20>
class Stack {
    T        stk[N];
    unsigned top = 0;
public:
    void        push(T x) { stk[top++] = x; }
    T           pop()     { return stk[--top]; }
    bool        empty()    const { return top == 0; }
    bool        full()     const { return top >= N; }
    unsigned    capacity() const { return N; }
};


void stack_of_ints() {
    Stack<>   s;
    for (auto k = 1; !s.full(); ++k) s.push(k);
    
    cout << "Stack<int>: ";
    while (!s.empty()) cout << s.pop() << " ";
    cout << endl;
}

void stack_of_string() {
    Stack<string, 10> s;
    for (auto k = s.capacity(); !s.full(); --k)
        s.push("#"s + to_string(k * k));

    cout << "Stack<string>: ";
    while (!s.empty()) cout << s.pop() << " ";
    cout << endl;
}

void stack_of_persons() {
    struct Person {
        string name;
        explicit Person(string name): name{move(name)} {}
        Person() = default;
        ~Person() = default;
        Person(const Person&) = default;
    };

    static vector<string> names = {
            "Anna"s, "Berit"s, "Carin"s, "Diana"s, "Eva"s, "Frida"s
    };
    Stack<Person, 10> s;
    for (auto k = 0; !s.full(); ++k)
        s.push(Person{names[k % names.size()]});

    cout << "Stack<Person>: ";
    while (!s.empty()) cout << s.pop().name << " ";
    cout << endl;
}

int main(int, char**) {
    stack_of_ints();
    stack_of_string();
    stack_of_persons();
    return 0;
}
