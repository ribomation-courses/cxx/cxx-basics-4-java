#include <iostream>
#include <vector>
#include <random>
#include "person.hxx"

using namespace std;
using namespace std::literals;
using namespace ribomation;

unsigned Person::numPersons = 0;
random_device                           r;
vector<string>                          names{
        "Anna"s, "Berit"s, "Carin"s, "Doris"s, "Eva"s, "Frida"s, "Greta"s
};
uniform_int_distribution<unsigned long> nextNameIndex{0, names.size() - 1};
uniform_int_distribution<unsigned>      nextAge{20, 80};

auto mkPerson() -> Person {
    return {names[nextNameIndex(r)], nextAge(r)};
}

auto mkPersonPointer() -> Person* {
    return new Person{names[nextNameIndex(r)], nextAge(r)};
}


int main() {
    auto const N = 5;
    {
        cout << "--- vector<Person> push_back() ---\n";
        auto      persons = vector<Person>{};
        for (auto k       = 0; k < N; ++k)
            persons.push_back(mkPerson());
        for (auto& p : persons) cout << "p: " << p << endl;
    }
    cout << "# persons = " << Person::count() << endl;

    {
        cout << "--- vector<Person> emplace_back() ---\n";
        auto persons = vector<Person>{};
        persons.reserve(N);
        for (auto k = 0; k < N; ++k)
            persons.emplace_back(names[nextNameIndex(r)], nextAge(r));
        for (auto& p : persons) cout << "p: " << p << endl;
    }
    cout << "# persons = " << Person::count() << endl;

    {
        cout << "--- vector<Person*> ---\n";
        vector<Person*> persons;
        for (auto       k = 0; k < N; ++k) persons.push_back(mkPersonPointer());
        for (auto& p : persons) cout << "p: " << *p << endl;
        while (!persons.empty()) {
            auto p = persons.back();
            persons.pop_back();
            delete p;
        }
    }
    cout << "# persons = " << Person::count() << endl;

    return 0;
}
