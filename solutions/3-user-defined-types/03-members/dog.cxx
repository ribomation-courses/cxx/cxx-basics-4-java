#include <iostream>
#include "dog.hxx"

namespace ribomation {
    using namespace std;
    using namespace std::literals;

    Dog::Dog(string name) : name(move(name)) {
        cout << "Dog{" << this->name << "} @ " << this << endl;
    }

    Dog::~Dog() {
        cout << "~Dog{" << this->name << "} @ " << this << endl;
    }

}
