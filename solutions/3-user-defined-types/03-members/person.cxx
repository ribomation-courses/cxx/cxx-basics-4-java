#include <iostream>
#include <sstream>
#include <utility>
#include "person.hxx"

namespace ribomation {
    using namespace std;
    using namespace std::literals;

    int Person::count = 0;

    Person::Person(string name, unsigned int age, Dog& dog)
            : name(move(name)), age(age), mydog(dog) {
        ++count;
        cout << "Person(" << this->name << ", " << this->age << ", " << this->mydog.getName() << ") @ " << this << endl;
    }

    Person::~Person() {
        --count;
        cout << "~Person(" << name << ") @ " << this << endl;
    }

    Person::Person(const Person& that) : name(that.name), age(that.age), mydog(that.mydog) {
        ++count;
        cout << "Person{& " << that.toString() << "} @ " << this << endl;
    }

    void Person::setName(const string& name) {
        Person::name = name;
    }

    void Person::setAge(unsigned int age) {
        Person::age = age;
    }

    string Person::toString() const {
        ostringstream buf{};
        buf << "Person{" << name << ", " << age << ", " << mydog.toString() << "} @ " << this;
        return buf.str();
    }


}
