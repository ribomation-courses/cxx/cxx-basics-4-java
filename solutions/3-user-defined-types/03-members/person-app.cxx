#include <iostream>
#include "person.hxx"

using namespace std;
using namespace std::literals;
using namespace ribomation;

void func(Person p) {
    cout << "[func] p: " << p.toString() << ", #pers=" << Person::instances() << endl;
}

int main() {
    cout << "[main] enter\n";
    cout << "[main] #persons = " << Person::instances() << endl;
    {
        Dog fido{"Fido"};
        fido.bark();

        Person nisse{"Nisse", 42, fido};
        cout << "person: " << nisse.toString() << endl;
        cout << "[main] #persons = " << Person::instances() << endl;

        func(nisse);

        cout << "[main] #persons = " << Person::instances() << endl;
    }
    cout << "[main] #persons = " << Person::instances() << endl;
    cout << "[main] exit\n";
    return 0;
}
