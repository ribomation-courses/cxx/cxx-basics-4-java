#pragma once

#include <iostream>
#include <sstream>
#include <string>

namespace ribomation {
    using namespace std;
    using namespace std::literals;

    class Dog {
        const string name;
    public:
        Dog(string name);
        ~Dog();
        Dog(const Dog&) = default;

        string getName() const {
            return name;
        }

        string toString() const {
            ostringstream buf{};
            buf << "Dog{" << name << "}/" << this;
            return buf.str();
        }

        void bark() const {
            cout << name << " says voff, voff\n";
        }
    };

}

