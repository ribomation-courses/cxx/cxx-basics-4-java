#pragma  once

#include <string>
#include "dog.hxx"

namespace ribomation {
    using namespace std;

    class Person {
        string   name;
        unsigned age;
        Dog& mydog;
        static int count;

    public:
        Person(string name, unsigned int age, Dog& dog);
        Person(const Person&);
        ~Person();

        string getName() const { return name; }
        unsigned getAge() const { return age; }

        void setName(const string& name);
        void setAge(unsigned int age);

        string toString() const;

        operator string() const {
            return name;
        }

        operator unsigned() const {
            return age;
        }

        static int instances() {
            return count;
        }
    };

}

