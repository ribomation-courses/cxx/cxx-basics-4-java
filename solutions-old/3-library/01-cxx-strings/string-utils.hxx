#pragma once
#include <string>
#include <cctype>

namespace ribomation {
    using namespace std;
    using namespace std::literals;

    string lowercase(string);
    string uppercase(string);
    string truncate(string, unsigned);
    string strip(string);

}
