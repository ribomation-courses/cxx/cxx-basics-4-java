#include <iostream>
#include <cassert>
#include "string-utils.hxx"

using namespace std;
using namespace std::literals;
using namespace ribomation;

int main() {
    assert(lowercase("ABC"s) == "abc"s);
    assert(uppercase("abc"s) == "ABC"s);

    assert(truncate("abcdef"s, 3) == "abc"s);
    assert(truncate("abc"s, 3) == "abc"s);
    assert(truncate("a"s, 3) == "a"s);

    assert(strip("aa bb1234cc#dd  \t "s) == "aabbccdd"s);

    cout << "All tests passed!\n";
    return 0;
}
