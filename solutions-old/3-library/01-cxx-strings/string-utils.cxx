#include "string-utils.hxx"

namespace ribomation {

    string lowercase(string s) {
        string r;
        for (auto ch : s) r += ::tolower(ch);
        return r;
    }

    string uppercase(string s) {
        string r;
        for (auto ch : s) r += ::toupper(ch);
        return r;
    }

    string truncate(string s, unsigned w) {
        if (s.size() <= w) return s;
        return s.substr(0, w);
    }

    string strip(string s) {
        string r;
        for (auto ch : s)
            if (::isalpha(ch)) r += ch;

        return r;
    }

}
