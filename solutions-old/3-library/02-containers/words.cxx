#include <iostream>
#include <string>
#include <vector>
using namespace std;
using namespace std::literals;

string strip(string s) {
    string    r;
    for (auto ch : s) if (::isalpha(ch)) r.push_back(ch);
    return r;
}

int main() {
    vector<string> words;
    for (string    word; cin >> word;) {
        word = strip(word);
        if (word.empty()) continue;
        words.push_back(word);
    }
    words.insert(words.begin(), "tjolla-hopp"s);

    for (auto& w: words) cout << "[" << w << "] ";
    cout << endl;

    unsigned totalLength = 0;
    for (auto& w: words) totalLength += w.size();
    cout << "total length: " << totalLength << endl;

    while (!words.empty()) {
        cout << "{" << words.back() << "} ";
        words.pop_back();
    }

    return 0;
}
