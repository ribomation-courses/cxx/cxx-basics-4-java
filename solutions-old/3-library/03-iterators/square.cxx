#include <iostream>
#include <iterator>
#include <algorithm>

using namespace std;
using namespace std::literals;


int main() {
    istream_iterator<int> in{cin};
    istream_iterator<int> eof{};
    ostream_iterator<int> out{cout, " "};

//    for (; in != eof; ++in, ++out) {
//        auto n = *in;
//        *out = n*n;
//    }
    transform(in, eof, out, [](auto n){ return n*n; });
    cout << endl;

    return 0;
}




