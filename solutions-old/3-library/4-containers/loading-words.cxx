#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <cctype>

using namespace std;
using namespace std::literals;


int main() {
    vector<string> words;

    for (string word; cin >> word;) words.push_back(word);
    cout << "words: ";
    for (auto& w : words) cout << w << " ";
    cout << endl;
    cout << "--------------------\n";

    for (auto& w : words) {
        for (auto k = 0U; k < w.size(); ++k) {
            if (!isalpha(w[k])) {
                w.erase(k++, 1);
            }
        }
    }
    cout << "words: ";
    for (auto& w : words) cout << w << " ";
    cout << endl;

    auto totalLength = 0;
    for (auto& w : words) totalLength += w.size();
    cout << "tot len: " << totalLength << endl;

    cout << "--------------------\n";
    words.insert(words.begin(), "TjollaHopp"s);
    while ( !words.empty()) {
        auto w = words.back();
        words.pop_back();
        cout << w << " ";
    }
    cout << endl;

    return 0;
}
