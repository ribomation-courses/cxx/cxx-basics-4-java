#include <iostream>
#include <sstream>
#include <iomanip>
#include <string>
#include <map>

using namespace std;

string lc(string s) {
    for (auto k = 0U; k < s.size(); ++k)
        s[k] = static_cast<char>(tolower(s[k]));
    return s;
}

string clean(string s) {
    for (auto k = 0U; k < s.size(); ++k) {
        s[k] = isalpha(s[k]) ? s[k] : ' ';
    }
    return s;
}

using WordFreqs = map<string, unsigned int>;

WordFreqs load(istream& in) {
    WordFreqs   wf;
    for (string word; in >> word;) {
        istringstream buf{clean(word)};
        for (string   w; buf >> w;) { ++wf[lc(w)]; }
    }
    return wf;
}

void dump(const WordFreqs& wf) {
    for (auto& [word, freq] : wf) {
        cout << setw(25) << setfill(' ') << left << word << ": ";
        cout << setw(3) << setfill('0') << right << freq << endl;
    }
}

int main() {
    WordFreqs freqs = load(cin);
    dump(freqs);
    return 0;
}
