#include <iostream>
#include <vector>
#include <string>

using namespace std;

inline unsigned stou(const string& s) {
    return static_cast<unsigned>(stoul(s));
}

int main(int numArgs, char* args[]) {
    const unsigned N = (numArgs <= 1) ? 100U : stou(args[1]);

    vector<unsigned> numbers;
    for (auto k = 2U; k <= N; ++k) {
        numbers.push_back(k);
    }

    for (auto k = 0U; k < numbers.size(); ++k) {
        auto prime = numbers[k];
        for (auto m = k + 1; m < numbers.size(); ++m) {
            if (numbers[m] % prime == 0) {
                auto pos = numbers.begin() + m;
                numbers.erase(pos);
            }
        }
    }

    for (auto p : numbers) { cout << p << " "; }
    cout << endl;

    return 0;
}
