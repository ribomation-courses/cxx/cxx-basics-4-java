#pragma  once

#include <stdexcept>
#include <string>
#include <vector>
#include <regex>
#include <cctype>

namespace ribomation {
    using namespace std;

    inline string lowercase(const string& s) {
        string    result;
        for (auto ch : s) result += ::tolower(ch);
        return result;
    }

    inline string uppercase(const string& s) {
        string    result;
        for (auto ch : s) result += ::toupper(ch);
        return result;
    }

    inline string capitalize(const string& s) {
        if (s.empty()) {
            throw invalid_argument("[capitalize] empty string");
        }
        string result;
        result += ::toupper(s[0]);
        return result + s.substr(1);
    }

    inline string strip(const string& s) {
        string    result;
        for (auto ch : s) if (::isalpha(ch)) result += ch;
        return result;
    }

    inline string truncate(const string& s, unsigned width) {
        if (s.size() < width) {
            return s;
        }
        return s.substr(0, width);
    }

    inline string left(const string& s, unsigned n, char p = ' ') {
        if (s.size() >= n) return truncate(s, n);
        return s + string(n - s.size(), p);
    }

    inline string right(const string& s, unsigned n, char p = ' ') {
        if (s.size() >= n) return truncate(s, n);
        return string(n - s.size(), p) + s;
    }

    inline string center(const string& s, unsigned n, char p = ' ') {
        if (s.size() >= n) return truncate(s, n);
        auto slack = n - s.size();
        return string(slack / 2, p) + s + string(slack / 2 + (slack % 2), p);
    }

    inline vector<string> split(const string& s, const regex& delim) {
        sregex_token_iterator first{s.begin(), s.end(), delim, -1};
        sregex_token_iterator last;
        return {first, last};
    }

    inline vector<string> split(const string& s, const string& delim) {
        return split(s, regex{delim});
    }

    inline string join(const vector<string>& vec, const string& delim) {
        string result;
        bool   first = true;
        for (auto& s : vec) {
            if (first) { first = false; } else { result += delim; }
            result += s;
        }
        return result;
    }

}
