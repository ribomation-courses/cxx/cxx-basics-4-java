#include <iostream>
#include <string>
#include <stdexcept>

using namespace std;

int main(int numArgs, char* args[]) {
    if (numArgs <= 1) {
        throw invalid_argument(string("usage: ") + args[0] + " {phrase} < {input}");
    }

    string   phrase = args[1];
    unsigned lineno = 1;
    for (string line; getline(cin, line); ++lineno) {
        if (line.find(phrase) != string::npos) {
            cout << lineno << ": " << line << endl;
        }
    }

    return 0;
}
