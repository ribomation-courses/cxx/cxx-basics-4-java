#include <iostream>
#include <cassert>
#include "string-utils.hxx"

using namespace std;

void test_lowercase() {
    using ribomation::lowercase;
    assert(lowercase("TjaBBa HaBBA") == "tjabba habba");
}

void test_uppercase() {
    using ribomation::uppercase;
    assert(uppercase("TjaBBa HaBBA") == "TJABBA HABBA");
}

void test_capitalize() {
    using ribomation::capitalize;
    assert(capitalize("howdy") == "Howdy");
}

void test_strip() {
    using ribomation::strip;
    assert(strip("  Hipp/happ;hopp\thePP") == "HipphapphopphePP");
}

void test_truncate() {
    using ribomation::truncate;
    assert(truncate("abc123", 3) == "abc");
    assert(truncate("abc123", 6) == "abc123");
    assert(truncate("abc123", 10) == "abc123");
}

void left_with_larger_should_chop_input() {
    using ribomation::left;
    assert(left("abcdef", 3) == "abc");
}

void left_with_smaller_should_pad_with_stars() {
    using ribomation::left;
    assert(left("abc", 6, '*') == "abc***");
}

void right_with_smaller_should_pad_with_stars() {
    using ribomation::right;
    assert(right("abc", 6, '*') == "***abc");
}

void center_with_even_slack_should_put_payload_in_middle() {
    using ribomation::center;
    assert(center("ab", 6, '*') == "**ab**");
}

void center_with_odd_slack_should_put_one_extra_at_end() {
    using ribomation::center;
    assert(center("ab", 7, '*') == "**ab***");
}

bool operator==(const vector<string>& lhs, const vector<string>& rhs) {
    auto left  = lhs.begin();
    auto right = rhs.begin();
    for (; left != lhs.end() && right != rhs.end(); ++left, ++right) {
        if (*left != *right) return false;
    }
    return true;
}

void split_should_return_vector_of_strings() {
    using ribomation::split;
    vector<string> expected{"hej", "hipp", "happ", "hopp"};
    assert(split("hej hipp 42happ...hopp!", "[^a-z]+") == expected);
}

void join_should_return_one_string() {
    using ribomation::join;
    vector<string> input{"hipp", "happ", "hopp"};
    string         separator = "-#-";
    assert(join(input, separator) == "hipp-#-happ-#-hopp");
}


int main() {
    test_lowercase();
    test_uppercase();
    test_capitalize();
    test_strip();
    test_truncate();
    left_with_larger_should_chop_input();
    left_with_smaller_should_pad_with_stars();
    right_with_smaller_should_pad_with_stars();
    center_with_even_slack_should_put_payload_in_middle();
    center_with_odd_slack_should_put_one_extra_at_end();
    split_should_return_vector_of_strings();
    join_should_return_one_string();
    cout << "All tests passed\n";
    return 0;
}

