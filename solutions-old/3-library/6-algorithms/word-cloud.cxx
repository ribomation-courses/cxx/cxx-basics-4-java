#include <iostream>
#include <sstream>
#include <fstream>
#include <iomanip>
#include <string>
#include <cctype>
#include <vector>
#include <map>
#include <unordered_map>
#include <algorithm>
#include <numeric>
#include <utility>
#include <random>

using namespace std;
using namespace std::string_literals;

using Frequencies = unordered_map<string, unsigned>;
using WordFreq    = pair<string, unsigned>;

Frequencies load(istream&, unsigned);
vector<WordFreq> mostFrequent(Frequencies, unsigned);
vector<string> extractWords(string, unsigned);
vector<string> toTags(vector<WordFreq> words);
string join(vector<string> strings, string delim);
string randColor();
random_device r;


int main(int argc, char** argv) {
    auto maxWords = 100U;
    auto minSize  = 5U;

    for (auto k = 1; k < argc; ++k) {
        string arg = argv[k];
        if (arg == "-w")
            maxWords = static_cast<unsigned>(stoi(argv[++k]));
        else if (arg == "-s")
            minSize = static_cast<unsigned>(stoi(argv[++k]));
    }

    const string htmlPrefix = R"(<html>
    <head>
        <title>Word Cloud</title>
        <style>
            body {font-family: sans-serif;}
            h1   {text-align: center; color: orange;}
        </style>
    </head>
    <body> <h1>Word Cloud</h1>
    )";
    const string htmlSuffix = "</body></html>";

    auto filename = "../musketeers.txt"s;
    ifstream file{filename};
    if (!file) throw invalid_argument{"cannot open file: "s + filename};

    auto freqs = load(file, minSize);
    auto words = mostFrequent(freqs, maxWords);
    auto tags  = toTags(words);
    shuffle(tags.begin(), tags.end(), r);
    cout << htmlPrefix << join(tags, "\n") << htmlSuffix << endl;

    return 0;
}

Frequencies load(istream& in, unsigned minSize) {
    Frequencies freqs;
    for (string line; getline(in, line);) {
        for (auto& w : extractWords(line, minSize)) { freqs[w]++; }
    }
    return freqs;
}

vector<WordFreq> mostFrequent(Frequencies freqs, unsigned int N) {
    vector<WordFreq> sortable{freqs.begin(), freqs.end()};

    sort(sortable.begin(), sortable.end(), [](auto left, auto right) {
        return left.second > right.second;
    });

    vector<WordFreq> mostFrequent;
    copy_n(sortable.begin(), N, back_inserter(mostFrequent));
    return mostFrequent;
}

vector<string> toTags(vector<WordFreq> words) {
    auto   maxFontSize = 150.0;
    auto   minFontSize = 10.0;
    auto   maxFreq     = words.front().second;
    auto   minFreq     = words.back().second;
    double scale       = (maxFontSize - minFontSize) / (maxFreq - minFreq);

    vector<string> tags;
    for (const auto&[word, freq] : words) {
        ostringstream buf;
        buf << "<span style='font-size:"
            << static_cast<unsigned>(freq * scale) << "px;"
            << "color:#" << randColor() << ";'>"
            << word
            << "</span>";
        tags.push_back(buf.str());
    }
    return tags;
}

string join(vector<string> strings, string delim) {
    return accumulate(strings.begin(), strings.end(), ""s, [=](auto acc, auto str) {
        return acc + delim + str;
    });
}

vector<string> extractWords(string line, unsigned minSize) {
    transform(line.begin(), line.end(), line.begin(), [](auto ch) {
        return isalpha(ch) ? tolower(ch) : ' ';
    });

    vector<string> result;
    istringstream  buf{line};
    for (string    word; buf >> word;) {
        if (word.size() >= minSize) {
            result.push_back(word);
        }
    }

    return result;
}

string randColor() {
    uniform_int_distribution<unsigned> color{0, 255};

    ostringstream buf;
    buf << hex << color(r) << color(r) << color(r);
    return buf.str();
}
