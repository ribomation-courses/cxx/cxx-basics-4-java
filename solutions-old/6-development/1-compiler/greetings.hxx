#pragma once

#include <string>

using namespace std;
using namespace std::literals;

class Greetings {
    const string msg = "Foobar strikes again"s;

public:
    Greetings();
    string message() const;
};

