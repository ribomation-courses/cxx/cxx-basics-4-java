#include <iostream>
#include <cmath>
#include "greetings.hxx"

int main() {
    Greetings g;
    cout << "[main] " << g.message() << endl;
    cout << "[main] PI=" << 4 * atan(1) << endl;
    return 0;
}
