#!/usr/bin/env bash
set -eu

RM="/bin/rm -rf"
MD="/bin/mkdir -p"
CXX_FLAGS="-std=c++17 -Wall -Wextra -Wpedantic -Werror -Wfatal-errors"
CC="g++ ${CXX_FLAGS} -c"
LD="g++ "
AR="ar crs"

# --- DIRS ---
SRC="./src"
BLD="./build"

# --- LIB ---
LIB_MEMBERS="factorial fibonacci sum"
LIB_NAME=functions
LIB_CXX="${SRC}/functions/cxx"
LIB_INC="${SRC}/functions/incl"
LIB_DIR="${BLD}/functions"
LIB_OBJ="${LIB_DIR}/objs"
LIB_FILE=lib${LIB_NAME}.a

echo "--- Building functions lib ----"
set -x
${RM} ${BLD}
${MD} ${LIB_OBJ}
for file in ${LIB_MEMBERS}; do
    ${CC} -I${LIB_INC} ${LIB_CXX}/${file}.cxx -o ${LIB_OBJ}/${file}.o
done
${AR} ${LIB_DIR}/${LIB_FILE} ${LIB_OBJ}/*.o
set +x

# --- APP ---
APP_MEMBERS="app"
APP_NAME=functions-demo
APP_CXX="${SRC}/app"
APP_DIR="${BLD}/app"
APP_OBJ="${APP_DIR}/objs"

echo "--- Building app ----"
set -x
${MD} ${APP_OBJ}
for file in ${APP_MEMBERS}; do
    ${CC} -I${LIB_INC} ${APP_CXX}/${file}.cxx -o ${APP_OBJ}/${file}.o
done
${LD}  ${APP_OBJ}/*.o  -o ${APP_DIR}/${APP_NAME} -L${LIB_DIR} -l${LIB_NAME}
tree ${BLD}

(set +x; echo "--- Running App ----")
${APP_DIR}/${APP_NAME} 45

