#include "functions.hxx"


unsigned long ribomation::fibonacci(unsigned n) {
    if (n <= 2) return 1;
    return fibonacci(n - 2) + fibonacci(n - 1);
}
