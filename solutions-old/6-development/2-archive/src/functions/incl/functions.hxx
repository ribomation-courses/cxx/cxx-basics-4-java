#pragma once

namespace ribomation {

    unsigned long fibonacci(unsigned n);
    unsigned long factorial(unsigned n);
    unsigned long sum(unsigned n);

}


