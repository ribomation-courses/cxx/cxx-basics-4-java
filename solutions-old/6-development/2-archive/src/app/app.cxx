#include <iostream>
#include <string>
#include "functions.hxx"

using namespace std;
using namespace ribomation;

int main(int numArgs, char** args) {
    auto n = (numArgs == 1) ? 42U : stoi(args[1]);
    cout << "--- Functions ---\n";
    cout << "sum(" << n << ") = " << sum(n) << endl;
    cout << "fac(" << n << ") = " << factorial(n) << endl;
    cout << "fib(" << n << ") = " << fibonacci(n) << endl;
    return 0;
}
