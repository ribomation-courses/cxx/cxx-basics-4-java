#pragma once

#include <iosfwd>
#include <string>

namespace ribomation {
    using namespace std;
    using namespace std::literals;

    class Person {
        string   name;
        unsigned age;

    public:
        Person(string name, unsigned age)
                : name{name}, age{age} {
            cout << "CREATE: Person(" << name << "," << age << ") @ " << this << endl;
        }

        ~Person() {
            cout << "DISPOSE: ~Person(" << name << ") @ " << this << endl;
        }

        string toString() const {
            return "Person{"s + name + ", "s + to_string(age) + "}"s;
        }

    };

}

