#include <iostream>
#include "person.hxx"

using namespace std;
using namespace std::literals;
using namespace ribomation;

Person global{"Per Silja"s, 37};

int main() {
    cout << "main() ENTER\n";
    cout << "global: " << global.toString() << " @ " << &global << endl;

    Person* heap = new Person{"Justin Time"s, 47};
    {
        Person local{"Anna Conda"s, 42};
        cout << "local: " << local.toString() << " @ " << &local << endl;
    }
    delete heap;

    cout << "main() EXIT\n";
    return 0;
}
