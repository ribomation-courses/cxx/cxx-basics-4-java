#include <iostream>

using namespace std;

using XXL = unsigned long long;
using ARG = unsigned short;
struct Result {
    ARG argument;
    XXL result;
};




constexpr XXL fibonacci(const unsigned n) {
    if (n == 0) return 0;
    if (n == 1) return 1;
    return fibonacci(n - 2) + fibonacci(n - 1);
}

template<typename T>
auto value(T x) {
    if constexpr (is_pointer<T>::value) return *x;
    if constexpr (is_arithmetic<T>::value) return x;
    throw "???";
}


int main(int argc, char** argv) {
    Result results[] = {
            {1,  fibonacci(1)},
            {2,  fibonacci(2)},
            {5,  fibonacci(5)},
            {10, fibonacci(10)},
            {15, fibonacci(15)},
            {20, fibonacci(20)},
            {30, fibonacci(30)},
            {35, fibonacci(35)},
            {40, fibonacci(40)},
            {45, fibonacci(45)},
    };

    for (auto[n, fib] : results)
        cout << "fib(" << n << ") = " << fib << endl;

    int n = argc == 1 ? 10 : stoi(argv[1]);
    cout << "fib(" << n << ") = " << fibonacci(n) << endl;

    int magic = 42;
    int* ptr = &magic;

    cout << "(1) int: " << value(magic++) << endl;
    cout << "(2) int* " << value(ptr) << endl;

    return 0;
}

