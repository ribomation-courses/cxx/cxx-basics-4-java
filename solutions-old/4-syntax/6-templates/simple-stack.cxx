#include <utility>

#include <iostream>
#include <string>
using namespace std;

template<typename T = int, unsigned N = 20>
class Stack {
    T        stk[N];
    unsigned top = 0;
public:
    void        push(T x) { stk[top++] = x; }
    T           pop()     { return stk[--top]; }
    bool        empty()    const { return top == 0; }
    bool        full()     const { return top >= N; }
    unsigned    capacity() const { return N; }
};


void stack_of_ints() {
    Stack<>   s;
    for (auto k = 1; !s.full(); ++k) s.push(k);
    while (!s.empty()) cout << s.pop() << " ";
    cout << endl;
}


void stack_of_string() {
    using namespace std::literals;
    Stack<string, 10> s;
    for (auto k = s.capacity(); !s.full(); --k) s.push("txt-"s + to_string(k));
    while (!s.empty()) cout << s.pop() << " ";
    cout << endl;
}



void stack_of_persons() {
    using namespace std::literals;
    
    struct Person {
        string name;
        explicit Person(string name): name{move(name)} {}
        Person() = default;
        ~Person() = default;
        Person(const Person&) = default;
    };

    Stack<Person, 5> s;
    for (auto k = s.capacity(); !s.full(); --k) 
        s.push(Person{"Nisse-"s + to_string(k)});
    while (!s.empty()) cout << s.pop().name << " ";
    cout << endl;
}

int main(int, char**) {
    stack_of_ints();
    stack_of_string();
    stack_of_persons();
    return 0;
}

