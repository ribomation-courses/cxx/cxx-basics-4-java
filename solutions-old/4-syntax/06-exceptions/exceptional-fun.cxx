#include <iostream>
#include <string>
#include <stdexcept>

using namespace std;
using namespace std::literals;

struct Dummy {
    const string name;

    Dummy(const string& name) : name(name) {
        cout << "ENTR Dummy(" << name << ") @ " << this << endl;
    }

    virtual ~Dummy() {
        cout << "EXIT Dummy(" << name << ") @ " << this << endl;
    }
};

long sum(unsigned n) {
    Dummy tracer{"sum("s + to_string(n) + ")"s};

    if (n <= 1) {
        cout << "sum(1) = 1" << endl;
        return 1;
    }
    if (n == 6) {
        cout << "THROW !!!!\n";
        throw logic_error{"Bazinga!"s};
    }

    auto result = n + sum(n - 1);
    cout << "sum(" << n << ") = " << result << endl;

    return result;
}


void useCase(unsigned n) {
    try {
        cout << "--- [use] sum(" << n << ") ---\n";
        auto result = sum(n);
        cout << "[use] sum(" << n << ") = " << result << endl;
    } catch (logic_error& err) {
        cout << "[use] err: " << err.what() << endl;
    }
}

int main() {
    cout << "[main] ENTR\n";

    useCase(5);
    useCase(10);

    cout << "[main] EXIT\n";
    return 0;
}
