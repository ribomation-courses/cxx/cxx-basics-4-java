cmake_minimum_required(VERSION 3.9)
project(1_classes)

set(CMAKE_CXX_STANDARD 17)
add_compile_options(-Wall -Wextra -Wpedantic  -Wfatal-errors)

add_executable(persons
        person.hxx person.cxx
        app.cxx
        )

add_executable(factory
        person.hxx person.cxx
        factory.cxx
        )
