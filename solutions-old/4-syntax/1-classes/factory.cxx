#include <iostream>
#include <string>
#include <random>
#include <vector>
#include "person.hxx"

using namespace std;
using namespace ribomation;

Person createPerson() {
    static vector<string> names{
            "Anna", "Berit", "Carin", "David", "Erik", "Fredrik"
    };

    static default_random_engine       r;
    uniform_int_distribution<size_t>   nextIdx{0, names.size() - 1};
    uniform_int_distribution<unsigned> nextAge{15, 95};

    return {names[nextIdx(r)], nextAge(r)};
}


int main(int argc, char** argv) {
    cout << "[main] enter\n";
    auto N = argc == 1 ? 5U : stoi(argv[1]);
    {
        vector<Person> pers;
        while (N-- > 0) { pers.push_back(createPerson()); }
        cout << "-----------\n";
        for (auto& p : pers) cout << p << endl;
        cout << "-----------\n";
    }

    auto p = new Person{"Inge Vidare", 17};
    cout << "p: " << *p << endl;
    delete p;

    cout << "[main] exit\n";
    return 0;
}
