#pragma once

#include <iostream>
#include <sstream>
#include <string>

namespace ribomation {
    using namespace std;

    class Person {
        const string name;
        unsigned     age = 0;
    public:
        Person(const string& name, unsigned age);
        ~Person();

        const string&   getName() const { return name; }
        unsigned int    getAge() const { return age; }
        void            setAge(unsigned int age) { Person::age = age; }
        string          toString() const;
    };

    ostream& operator<<(ostream& os, const Person& person);

}
