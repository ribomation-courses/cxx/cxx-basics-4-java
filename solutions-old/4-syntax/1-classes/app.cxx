#include <iostream>
#include "person.hxx"

using namespace std;
using namespace ribomation;

Person global{"Global Object", 20};

void func() {
    Person local{"Local Object", 30};
}

int main() {
    cout << "[main] enter\n";
    Person* heap = new Person{"Heap Object", 40};
    func();
    delete heap;
    cout << "[main] exit\n";
    return 0;
}
