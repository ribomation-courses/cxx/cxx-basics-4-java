
#include "person.hxx"

namespace ribomation {
    
    Person::Person(const std::string& name, unsigned age) : name{name}, age{age} {
        cout << "NEW " << toString() << " @ " << this << endl;
    }

    Person::~Person() {
        cout << "END " << toString() << " @ " << this << endl;
    }

    string Person::toString() const {
        ostringstream buf;
        buf << "Person{" << name << ", " << age << "}";
        return buf.str();
    }

    std::ostream& operator<<(std::ostream& os, const Person& person) {
        return os << person.toString();
    }

}
