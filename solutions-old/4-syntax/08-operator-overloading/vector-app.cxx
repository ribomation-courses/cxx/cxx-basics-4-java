#include <iostream>
#include "vector-3d.hxx"

using namespace std;
using namespace std::literals;
using namespace ribomation;

int main() {
    Vector3d v{1, 2, 3};
    cout << "v: " << v << endl;

    cout << "10 * v: " << (10 * v) << endl;
    cout << "v * 10: " << (v * 10) << endl;

    return 0;
}

