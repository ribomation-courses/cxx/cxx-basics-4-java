#pragma once

#include <iosfwd>

namespace ribomation {
    using namespace std;
    using AxisType = double;

    class Vector3d {
        AxisType x, y, z;

    public:
        Vector3d(AxisType x, AxisType y, AxisType z)
                : x{x}, y{y}, z{z} {}

        AxisType getX() const { return x; }

        AxisType getY() const { return y; }

        AxisType getZ() const { return z; }
    };

    ostream& operator <<(ostream& os, const Vector3d v) {
        return os << "[" << v.getX() << ", " << v.getY() << ", " << v.getZ() << "]";
    }

    Vector3d operator *(int factor, const Vector3d& v) {
        return {factor * v.getX(), factor * v.getY(), factor * v.getZ()};
    }

    Vector3d operator *(const Vector3d& v, int factor) {
        return factor * v;
    }

}
