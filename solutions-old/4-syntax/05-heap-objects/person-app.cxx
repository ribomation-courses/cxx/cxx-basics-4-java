#include <iostream>
#include <vector>
#include "person.hxx"

using namespace std;
using namespace std::literals;
using namespace ribomation;

unsigned Person::numPersons = 0;

auto mkPerson() -> Person {
    static vector<string> names{
            "Anna"s, "Berit"s, "Carin"s, "Doris"s, "Eva"s, "Frida"s, "Greta"s
    };
    static unsigned       nextNameIndex = 0;
    string& name = names[nextNameIndex];
    nextNameIndex = (nextNameIndex + 1) % names.size();

    static unsigned nextAge = 15;
    unsigned        age     = nextAge;
    nextAge = (2 * nextAge + 1) % 99 + 10;

    return {name, age};
}

auto mkPersonPointer() -> Person* {
    static vector<string> names{
            "Anna"s, "Berit"s, "Carin"s, "Doris"s, "Eva"s, "Frida"s, "Greta"s
    };
    static unsigned       nextNameIndex = 0;
    string& name = names[nextNameIndex];
    nextNameIndex = (nextNameIndex + 1) % names.size();

    static unsigned nextAge = 15;
    unsigned        age     = nextAge;
    nextAge = (2 * nextAge + 1) % 99 + 10;

    return new Person{name, age};
}


int main() {
    auto const N = 5;
    {
        vector<Person> persons;
        for (auto      k = 0; k < N; ++k) persons.push_back(mkPerson());
        for (auto& p : persons) cout << "p: " << p.toString() << endl;
    }
    cout << "# persons = " << Person::count() << endl;

    {
        vector<Person*> persons;
        for (auto       k = 0; k < N; ++k) persons.push_back(mkPersonPointer());
        for (auto& p : persons) cout << "p: " << p->toString() << endl;
        while (!persons.empty()) {
            auto p = persons.back();
            persons.pop_back();
            //delete p;
        }
    }
    cout << "# persons = " << Person::count() << endl;

    return 0;
}
