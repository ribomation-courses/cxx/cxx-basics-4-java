#pragma once

#include <string>

namespace ribomation {
    using namespace std;
    using namespace std::literals;

    class Person {
        string          name;
        unsigned        age;
        static unsigned numPersons;

    public:
        Person(string name, unsigned age = 0)
                : name{name}, age{age} {
            cout << "CREATE: Person(" << name << "," << age << ") @ " << this << endl;
            ++numPersons;
        }

        Person(const Person& other)
                : name{other.name}, age{other.age} {
            cout << "COPY: Person(" << name << "," << age << ") @ " << this << endl;
            ++numPersons;
        }

        ~Person() {
            --numPersons;
            cout << "DISPOSE: ~Person(" << name << ") @ " << this << endl;
        }

        string toString() const {
            return "Person{"s + name + ", "s + to_string(age) + "}"s
            + " ("s + to_string(numPersons) + ")"s;
        }

        static unsigned count() { return numPersons; }
    };

}

