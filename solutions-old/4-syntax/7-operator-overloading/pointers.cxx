#include <iostream>
#include <string>
#include "Ptr.hxx"


struct Whatever {
    string name;

    Whatever(const string& s) : name(s) {
        cout << "Whatever{" << name << "} @ " << this << endl;
    }

    ~Whatever() {
        cout << "~Whatever{" << name << "} @ " << this << endl;
    }

    string toString() const {
        return "WE{" + name + "}";
    }
};


int main() {
    Ptr<Whatever> p{new Whatever{"Anna Conda"}};
    cout << "p: " << p->toString() << endl;  // ( p.operator ->() )->toString()

    Ptr<Whatever> q{new Whatever{"Per Silja"}};
    cout << "q: " << (*q).toString() << endl;  // ( p.operator *() ).toString()

    {
        Ptr<Whatever> r;
        r = p;
        cout << "r: " << r->toString() << endl;
        try {
            cout << "p: " << p->toString() << endl;
        } catch (invalid_argument& x) {
            cout << "*** p: " << x.what() << endl;
        }
    }

    cout << "q: " << q->toString() << endl;
    q = nullptr;
    try {
        cout << "q: " << q->toString() << endl;
    } catch (invalid_argument& x) {
        cout << "*** q: " << x.what() << endl;
    }
}
