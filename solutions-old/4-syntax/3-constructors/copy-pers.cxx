#include <iostream>
#include <string>
#include "person.hxx"

using namespace std;
using namespace ribomation;

Person func(Person p) {
    p.setAge(p.getAge() + 10);
    cout << "[func] p: " << p << endl;
    return p;
}

int main(int, char**) {
    {
        Person nisse{"Nisse", 42};
        cout << "person: " << nisse << endl;

        cout << "---Copy------------\n";
        Person q = func(nisse);
        cout << "q: " << q << endl;

        cout << "---Type Construct------------\n";
        Person r = func("Olle");
        cout << "r: " << r << endl;

        cout << "---Type Operator------------\n";
        string s = nisse;
        cout << "s: " << s << endl;

        unsigned u = nisse;
        cout << "u: " << u << endl;

        cout << "---Dispose------------\n";
    }
    cout << "# pers: " << Person::count() << endl;
    return 0;
}
