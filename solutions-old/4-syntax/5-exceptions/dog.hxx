#pragma once

#include <iostream>
#include <sstream>
#include <string>

namespace ribomation {
    using namespace std;

    class Dog {
        const string name;

    public:
        Dog(const string& name) : name(name) {
            cout << "NEW " << toString() << " @ " << this << endl;
        }

        ~Dog() {
            cout << "END " << toString() << " @ " << this << endl;
        }

        const Dog& bark() const {
            cout << name << ": BARK!" << endl;
            return *this;
        }

        string toString() const {
            ostringstream buf;
            buf << "Dog{" << name << "}";
            return buf.str();
        }
    };

    inline ostream& operator<<(ostream& os, const Dog& obj) {
        return os << obj.toString();
    }

}
