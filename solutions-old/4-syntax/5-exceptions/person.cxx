
#include "person.hxx"

namespace ribomation {

    Person::Person(const std::string& name, unsigned age, Dog* mydog) : name{name}, age{age}, mydog{mydog} {
        ++numPersons;
        cout << "NEW " << toString() << " @ " << this << endl;
    }

    Person::Person(const char* name) : name{name}, age{37}, mydog{nullptr} {
        ++numPersons;
        cout << "ITC " << toString() << " @ " << this << endl;
    }

    Person::Person(const Person& that) : name{that.name}, age{that.age}, mydog{that.mydog} {
        ++numPersons;
        cout << "CPY " << toString() << " @ " << this << endl;
    }

    Person::~Person() {
        cout << "END " << toString() << " @ " << this << endl;
        --numPersons;
    }

    string Person::toString() const {
        ostringstream buf;
        buf << "Person{" << name << ", " << age
            << " [" << numPersons << "]} + "
            << (mydog ? mydog->toString() : "NO dog");
        return buf.str();
    }

    std::ostream& operator<<(std::ostream& os, const Person& person) {
        return os << person.toString();
    }

    unsigned Person::numPersons = 0;
}
