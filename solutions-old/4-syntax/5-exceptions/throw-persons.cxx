#include <iostream>
#include <string>
#include <random>
#include <vector>
#include <stdexcept>
#include "person.hxx"

using namespace std;
using namespace std::literals;
using namespace ribomation;


Person createPerson() {
    static vector<string>        names{
            "Anna", "Berit", "Carin", "David", "Erik", "Fredrik"
    };
    static default_random_engine r;

    uniform_int_distribution<size_t>   nextIdx{0, names.size() - 1};
    uniform_int_distribution<unsigned> nextAge{15, 95};

    return {names[nextIdx(r)], nextAge(r)};
}


void recursiveFunction(int n) {
    cout << "recursiveFunction(" << n << ")\n";
    auto p{createPerson()};
    //auto ptr = new int{666};

    if (n <= 1) {
        cout << "# pers: " << Person::count() << endl;
        cout << "*** THROW ***\n";
        throw logic_error{"Bazinga"s};
    }

    //delete ptr;
    recursiveFunction(n - 1);
}


int main(int numArgs, char** args) {
    int N = numArgs == 1 ? 5 : stoi(args[1]);

    for (auto k = 1; k <= N; ++k) {
        cout << "----" << k << "----------\n";
        try {
            recursiveFunction(2 * k);
        } catch (exception& e) {
            cout << "ERR: " << e.what() << endl;
        }
        cout << "# pers: " << Person::count() << endl;
    }

    return 0;
}
