#include <iostream>
#include <string>
#include <random>
#include <vector>
#include "person.hxx"

using namespace std;
using namespace ribomation;


Person* createPerson() {
    static vector<string>        names{
            "Anna", "Berit", "Carin", "David", "Erik", "Fredrik"
    };
    static default_random_engine r;

    uniform_int_distribution<size_t>   nextIdx{0, names.size() - 1};
    uniform_int_distribution<unsigned> nextAge{15, 95};

    return new Person{names[nextIdx(r)], nextAge(r)};
}

int main(int numArgs, char** args) {
    int N = numArgs == 1 ? 5 : stoi(args[1]);

    vector<Person*> pers;
    while (N-- > 0) pers.push_back(createPerson());
    cout << "# pers: " << Person::count() << endl;

    cout << "-------------\n";
    for (auto p : pers) cout << *p << endl;
    cout << "-------------\n";

    for (auto p : pers) delete p;
    cout << "# pers: " << Person::count() << endl;

    return 0;
}
