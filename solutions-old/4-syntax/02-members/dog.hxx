#pragma once

#include <iostream>
#include <string>

namespace ribomation {
    using namespace std;
    using namespace std::literals;

    class Dog {
        string name;
    public:
        Dog(string name) : name{name} {
            cout << "CREATE: Dog(" << name << ") @ " << this << endl;
        }

        ~Dog() {
            cout << "DISPOSE: ~Dog(" << name << ") @ " << this << endl;
        }

        string toString() const {
            return "Dog{"s + name + "}"s;
        }

        void bark() {
            cout << name << ": bark bark woff woff\n";
        }
    };

}

