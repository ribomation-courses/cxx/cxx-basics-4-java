#pragma once

#include <string>
#include "dog.hxx"

namespace ribomation {
    using namespace std;
    using namespace std::literals;

    class Person {
        string   name;
        unsigned age;
        Dog* myDog = nullptr;

    public:
        Person(string name, unsigned age)
                : name{name}, age{age} {
            cout << "CREATE: Person(" << name << "," << age << ") @ " << this << endl;
        }

        ~Person() {
            cout << "DISPOSE: ~Person(" << name << ") @ " << this << endl;
        }

        string toString() const {
            return "Person{"s + name + ", "s + to_string(age) + "}"s
                   + (myDog != nullptr ? " has "s + myDog->toString() : ""s);
        }

        Dog* getMyDog() const {
            return myDog;
        }

        void setMyDog(Dog* myDog) {
            Person::myDog = myDog;
        }
    };

}

