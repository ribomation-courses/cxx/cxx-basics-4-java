#include <iostream>
#include "person.hxx"

using namespace std;
using namespace std::literals;
using namespace ribomation;

int main() {
    Person anna{"Anna Gram"s, 42};
    cout << "anna: " << anna.toString() << endl;

    Dog fido{"Fido"};
    cout << "fido: " << fido.toString() << endl;

    anna.setMyDog(&fido);
    cout << "anna: " << anna.toString() << endl;

    Person per{"Per Silja", 45};
    per.setMyDog(&fido);
    cout << "per: " << per.toString() << endl;

    anna.setMyDog(nullptr);
    cout << "anna: " << anna.toString() << endl;
    cout << "per: " << per.toString() << endl;

    return 0;
}
