#include <iostream>
#include <string>
#include <random>
#include <vector>
#include "person.hxx"

using namespace std;
using namespace ribomation;

void func() {
    cout << "[func] # pers: " << Person::count() << endl;
    Person anna{"Anna",10};
    Person berit{"Berit",20};
    Person carin{"Carin",30};
    cout << "[func] # pers: " << Person::count() << endl;
}

int main(int, char**) {
    Dog fido{"Fido"};
    fido.bark().bark().bark();

    cout << "# pers: " << Person::count() << endl;
    {
        Person nisse{"Nisse", 42, &fido};
        cout << "# pers: " << Person::count() << endl;
        cout << "person: " << nisse << endl;
        func();
    }
    cout << "# pers: " << Person::count() << endl;

    return 0;
}
