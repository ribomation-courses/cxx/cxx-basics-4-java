
#include "person.hxx"

namespace ribomation {

    Person::Person(const std::string& name, unsigned age, Dog* mydog /*Dog& mydog*/) : name{name}, age{age}, mydog{mydog} {
        ++numPersons;
        cout << "NEW " << toString() << " @ " << this << endl;
    }

    Person::~Person() {
        --numPersons;
        cout << "END " << toString() << " @ " << this << endl;
    }

    string Person::toString() const {
        ostringstream buf;
        buf << "Person{" << name << ", " << age << "} + " << (mydog ? mydog->toString() /*mydog.toString()*/ : "NO dog");
        return buf.str();
    }

    std::ostream& operator<<(std::ostream& os, const Person& person) {
        return os << person.toString();
    }

    unsigned Person::numPersons = 0;
}
