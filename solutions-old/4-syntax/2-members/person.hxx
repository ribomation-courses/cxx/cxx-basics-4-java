#pragma once

#include <iostream>
#include <sstream>
#include <string>
#include "dog.hxx"

namespace ribomation {
    using namespace std;

    class Person {
        const string name;
        unsigned     age = 0;
//        Dog& mydog;
        Dog* mydog;
        static unsigned numPersons;

    public:
//        Person(const string& name, unsigned age, Dog& mydog);
        Person(const string& name, unsigned age = 42, Dog* mydog = nullptr);

        ~Person();

        const string& getName() const { return name; }

        unsigned int getAge() const { return age; }

        void setAge(unsigned int age) { Person::age = age; }

        string toString() const;

        static unsigned count() { return numPersons; }
    };

    ostream& operator<<(ostream& os, const Person& person);

    //unsigned Person::numPersons = 0;
}

