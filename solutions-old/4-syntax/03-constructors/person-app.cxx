#include <iostream>
#include "person.hxx"

using namespace std;
using namespace std::literals;
using namespace ribomation;

void print(Person p) {
    cout << "[print] " << p.toString() << endl;
}

int main() {
    Person person{"Inge Vidare"s, 42};

    cout << "--- [1] call print()\n";
    print(person);
    cout << "--- [1] call print() done\n";

    cout << "--- [2] call print()\n";
    print("Åke R Vidare"s);
    cout << "--- [2] call print() done\n";

    cout << "--- [3] convert to string\n";
    string s = person;  //person.operator string()
    cout << "s: " << s << endl;

    return 0;
}
