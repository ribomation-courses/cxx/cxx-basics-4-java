#pragma once

#include <string>

namespace ribomation {
    using namespace std;
    using namespace std::literals;

    class Person {
        string   name;
        unsigned age;

    public:
        Person(string name, unsigned age = 0)
                : name{name}, age{age} {
            cout << "CREATE: Person(" << name << "," << age << ") @ " << this << endl;
        }

        Person(const Person& other)
                : name{other.name}, age{other.age} {
            cout << "COPY: Person(" << name << "," << age << ") @ " << this << endl;
        }

        ~Person() {
            cout << "DISPOSE: ~Person(" << name << ") @ " << this << endl;
        }

        operator string() const {
            return name;
        }

        string toString() const {
            return "Person{"s + name + ", "s + to_string(age) + "}"s;
        }
    };

}

