#include <iostream>
#include <iomanip>
#include <string>

using namespace std;
using namespace std::literals;

template<typename ArgType>
bool equals(ArgType a, ArgType b) {
    return a == b;
}

template<typename ArgType>
bool equals(ArgType a, ArgType b, ArgType c) {
    return equals(a, b) && equals(b, c);
}

int main() {
    cout << boolalpha;
    cout << "equals(2*21, 2*3*7, 42)  = " << equals(2 * 21, 2 * 3 * 7, 42) << endl;
    cout << "equals(a+b+x, a+bc, abc) = " << equals("a"s + "b" + "c"s, "a"s + "bc"s, "abc"s) << endl;

    return 0;
}


