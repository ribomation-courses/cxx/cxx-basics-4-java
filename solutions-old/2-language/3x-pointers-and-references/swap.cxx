#include <iostream>

using namespace std;

void swap(int& a, int& b) {
    int t = a;
    a = b;
    b = t;
}

int main() {
    int x = 10, y = 50;

    cout << "[before] " << x << " / " << y << endl;
    swap(x, y);
    cout << "[after]  " << x << " / " << y << endl;

    return 0;
}