#include <iostream>

using namespace std;

int* mulBy10(int* n) {
    *n *= 10;
    return n;
}

int main() {
    int N = 42;

    cout << "[before] N = " << N << endl;
    cout << "[after]  N = " << *mulBy10(&N) << endl;

    return 0;
}
