#include <iostream>
#include <string>
#include <functional>
#include <algorithm>

using namespace std;

void print(const string& name, int a[], unsigned n) {
    cout << name << ": ";
    for_each(a, a + n, [](auto x) { cout << x << " "; });
    cout << endl;
}

long reduce(int a[], unsigned n, function<long(long, int)> f) {
    long      result = a[0];
    for (auto k = 1U; k < n; ++k) result = f(result, a[k]);
    return result;
}

int main() {
    int        numbers[]    = {1, 2, 3, 4, 5, 6};
    const auto numbers_size = sizeof(numbers) / sizeof(numbers[0]);
    print("[before]", numbers, numbers_size);

    const int factor = 42;
    transform(numbers, numbers + numbers_size, numbers, [=](auto x) { return x * factor; });
    print("[after] ", numbers, numbers_size);

    cout << "SUM : " << reduce(numbers, numbers_size, [](auto acc, auto x) { return acc + x; }) << endl;
    cout << "PROD: " << reduce(numbers, numbers_size, [](auto acc, auto x) { return acc * x; }) << endl;
    cout << "MAX : " << reduce(numbers, numbers_size, [](auto acc, auto x) { return acc < x ? x : acc; }) << endl;

    return 0;
}
