cmake_minimum_required(VERSION 3.9)
project(1_similarities)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_FLAGS "-Wall -Wextra -Wpedantic -Werror -Wfatal-errors")

add_executable(numbers numbers.cxx)
