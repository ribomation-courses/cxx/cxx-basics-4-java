#include <iostream>
#include <limits>

using namespace std;

int main() {
    auto sum    = 0;
    auto count  = 0U;
    auto minVal = numeric_limits<int>::max();
    auto maxVal = numeric_limits<int>::lowest();
    do {
        cout << "Give a number (end with 0): ";
        auto number = 0;
        cin >> number;
        if (number == 0) break;

        sum += number;
        ++count;
        minVal = ::min(number, minVal);
        maxVal = ::max(number, maxVal);
    } while (true);

    cout << "Count  : " << count << endl;
    cout << "Mean   : " << (count > 0 ? static_cast<double>(sum) / count : 0) << endl;
    cout << "Min/Max: " << minVal << " / " << maxVal << endl;

    int arr[5];
    int idx = 0;

    arr[++idx]
    arr[idx++]


    return 0;
}
