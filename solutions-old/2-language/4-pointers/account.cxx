#include <iostream>

using namespace std;

struct Account {
    int   balance;
    float rate;
};

int main() {
    Account a = {250, 1.5};
    cout << "acc: balance=" << a.balance << "kr, rate=" << a.rate <<"%" << endl;

    Account* ptr = &a;
    cout << "ptr: balance=" << (*ptr).balance << "kr, rate=" << ptr->rate <<"%" << endl;

    ptr->balance *= 10;
    ptr->rate *= 2;
    cout << "ptr: balance=" << ptr->balance << "kr, rate=" << ptr->rate <<"%" << endl;
    cout << "acc: balance=" << a.balance << "kr, rate=" << a.rate <<"%" << endl;

    return 0;
}
