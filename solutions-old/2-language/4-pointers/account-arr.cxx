#include <iostream>

using namespace std;

struct Account {
    int   balance;
    float rate;
};

int main() {
    Account    arr[] = {
            {100, 1},
            {200, 1.5},
            {300, 2},
            {400, 2.5},
            {500, 3},
    };
    const auto N     = sizeof(arr) / sizeof(arr[0]);

    cout << "--- accounts ---\n";
    for (Account* ptr = arr; ptr != (arr + N); ++ptr)
        cout << "balance=" << ptr->balance << "kr, rate=" << ptr->rate << "%" << endl;
    cout << endl;

    for (Account* ptr = arr; ptr != (arr + N); ++ptr)
        ptr->balance *= (1 + ptr->rate / 100.0);

    cout << "--- accounts ---\n";
    for (Account* ptr = arr; ptr != (arr + N); ++ptr)
        cout << "balance=" << ptr->balance << "kr, rate=" << ptr->rate << "%" << endl;
    cout << endl;

    return 0;
}
