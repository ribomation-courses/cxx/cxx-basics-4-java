#include <iostream>
using namespace std;

int main() {
    int        arr[] = {1, 1, 2, 3, 5, 8, 13};
    const auto N     = sizeof(arr) / sizeof(arr[0]);

    cout << "arr: ";
    for (auto k = 0U; k < N; ++k) cout << arr[k] << " ";
    cout << endl;

    cout << "ptr: ";
    for (int* ptr = arr; ptr < (arr + N); ++ptr) cout << *ptr << " ";
    cout << endl;

    for (int* ptr = arr; ptr != (arr + N); ++ptr) *ptr *= 10;

    cout << "ptr: ";
    for (int* ptr = arr; ptr != (arr + N); ++ptr) cout << *ptr << " ";
    cout << endl;

    cout << "arr: ";
    for (auto k = 0U; k < N; ++k) cout << arr[k] << " ";
    cout << endl;

    return 0;
}
