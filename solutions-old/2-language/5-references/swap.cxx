#include <iostream>
#include <string>
using namespace std;

void swap(int& a, int& b) {
    int t = a;
    a = b;
    b = t;
}

void swap(int* a, int* b) {
    int t = *a;
    *a = *b;
    *b = t;
}

int main(int argc, char** argv) {
    int num1 = (argc >= 2) ? stoi(argv[1]) : 10;
    int num2 = (argc >= 3) ? stoi(argv[2]) : 20;

    cout << "[before] num-1=" << num1 << ", num-2=" << num2 << endl;
    swap(num1, num2);
    cout << "[after]  num-1=" << num1 << ", num-2=" << num2 << endl;
    swap(&num1, &num2);
    cout << "[after2] num-1=" << num1 << ", num-2=" << num2 << endl;

    return 0;
}