#include <iostream>

using namespace std;
using namespace std::literals;

int main() {
    int        numbers[] = {1, 2, 3, 4, 5};
    const auto N         = sizeof(numbers) / sizeof(numbers[0]);

    for (auto k = 0U; k < N; ++k) {
        cout << "numbers[" << k << "] = " << numbers[k] << endl;
    }

    for (auto n : numbers) { cout << n << " "; }
    cout << endl;

    for (int* ptr = &numbers[0]; ptr < (numbers + N); ++ptr) {
        cout << *ptr << " ";
    }
    cout << endl;

    for (int* ptr = &numbers[0]; ptr < (numbers + N); ++ptr) {
        *ptr *= 25;
    }
    for (int* ptr = &numbers[0]; ptr < (numbers + N); ++ptr) {
        cout << *ptr << " ";
    }
    cout << endl;

    for (auto k = 0U; k < N; ++k) {
        cout << "numbers[" << k << "] = " << numbers[k] << endl;
    }

    return 0;
}

