#include <iostream>

using namespace std;
using namespace std::literals;

int main() {
    int theValue = 42;
    cout << "the value: " << theValue << endl;

    int* thePointer = &theValue;
    cout << "the pointer: " << thePointer << ", " << *thePointer << endl;

    *thePointer *= 10;
    cout << "the value: " << theValue << endl;

    int anotherValue = 17;
    thePointer = &anotherValue;
    cout << "the pointer: " << thePointer << ", " << *thePointer << endl;

    *thePointer *= 10;
    cout << "the value: " << theValue << endl;
    cout << "another value: " << anotherValue << endl;

    return 0;
}

