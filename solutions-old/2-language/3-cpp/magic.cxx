#include "magic.hxx"

#ifndef MAGIC_NUMBER
#define MAGIC_NUMBER 42
#endif

int magic() {
    return MAGIC_NUMBER;
}
