#!/usr/bin/env bash
set -e
set -u
CXX_FLAGS="-std=c++17 -Wall -Wextra -Werror -Wfatal-errors"

set -x
g++ ${CXX_FLAGS} magic.cxx app.cxx -o magic && ./magic
g++ ${CXX_FLAGS} -DMAGIC_NUMBER=21 magic.cxx app.cxx -o magic && ./magic
g++ ${CXX_FLAGS} -DMAGIC_NUMBER=84 magic.cxx app.cxx -o magic && ./magic
