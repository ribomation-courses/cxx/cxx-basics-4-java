# How to compile & link on the command-line

    mkdir -p bld && cd bld
    g++ -std=c++17 -Wall -DMAGIC_NUMBER=10 -c ../magic.cxx
    g++ -std=c++17 -Wall -c ../using-magic.cxx
    g++ -std=c++17 -Wall -c ../app.cxx
    g++ *.o -o magic-app
    ./magic-app
