#include <iostream>
#include "magic.hxx"
#include "using-magic.hxx"
using namespace std;

int main() {
    cout << "magic = " << magic() << endl;
    cout << "use magic = " << useMagic() << endl;
    return 0;
}
