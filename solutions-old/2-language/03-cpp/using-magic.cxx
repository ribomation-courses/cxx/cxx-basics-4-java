#include "magic.hxx"

int useMagic() {
    auto n = magic();
    return n * n;
}

