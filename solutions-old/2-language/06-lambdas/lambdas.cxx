#include <iostream>
#include <algorithm>
#include <iterator>
using namespace std;

int main() {
    int        numbers[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    const auto N         = sizeof(numbers) / sizeof(numbers[0]);

    const auto factor = 42;
    transform(&numbers[0], &numbers[N], &numbers[0], [=](auto n){
        return n * factor;
    });

    for_each(numbers, numbers + N, [](auto n) {
        cout << n << " ";
    }); cout << endl;

    for_each(begin(numbers), end(numbers), [](auto n) {
        cout << n << " ";
    }); cout << endl;
    return 0;
}
