#pragma once

namespace ribomation {
    using XXL = unsigned long long;
    XXL fibonacci(unsigned);
    extern bool useRecursion;
}
