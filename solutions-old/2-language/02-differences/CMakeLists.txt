cmake_minimum_required(VERSION 3.12)
project(02_differences)

set(CMAKE_CXX_STANDARD 17)

add_executable(fibonacci
        fibonacci.hxx
        fibonacci.cxx
        app.cxx
        )
