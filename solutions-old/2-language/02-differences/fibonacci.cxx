#include "fibonacci.hxx"

namespace {
    using namespace ribomation;

    XXL fibonacciByRecursion(unsigned n) {
        return (n <= 2) ? 1 : fibonacci(n - 2) + fibonacci(n - 1);
    }

    XXL fibonacciByIteration(unsigned n) {
        XXL fib_n2 = 0;
        XXL fib_n1 = 1;
        while (--n > 0) {
            XXL fib = fib_n2 + fib_n1;
            fib_n2 = fib_n1;
            fib_n1 = fib;
        }
        return fib_n1;
    }
}

namespace ribomation {
    bool useRecursion = true;

    unsigned long long fibonacci(unsigned n) {
        if (useRecursion) return fibonacciByRecursion(n);
        return fibonacciByIteration(n);
    }
}
