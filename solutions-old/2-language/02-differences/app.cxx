#include <iostream>
#include <string>
#include "fibonacci.hxx"

using namespace std;
using namespace ribomation;

int main(int numArgs, char* args[]) {
    int N = 90;
    if (numArgs > 1) {
        N = stoi(args[1]);
    }

    ribomation::useRecursion = false;
    auto result = fibonacci(N);
    cout << "fibonacci(" << N << ") = " << result << endl;

    return 0;
}
