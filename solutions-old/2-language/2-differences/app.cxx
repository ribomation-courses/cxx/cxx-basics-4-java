#include <iostream>
#include <string>
#include "fibonacci.hxx"

using namespace std;
using namespace ribomation;

int main(int numArgs, char** args) {
    auto n = numArgs == 1 ? 42U : stoi(args[1]);

    cout << "fibonacci(" << n << ") = " << fibonacci(n) << endl;

    return 0;
}
