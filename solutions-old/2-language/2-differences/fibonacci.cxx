
#include "fibonacci.hxx"

namespace ribomation {
    unsigned long long fibonacci(unsigned n) {
        return n <= 2 ? 1 : fibonacci(n - 2) + fibonacci(n - 1);
    }
}
