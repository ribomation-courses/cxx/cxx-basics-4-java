cmake_minimum_required(VERSION 3.10)
project(SmartPointers)

set(CMAKE_CXX_STANDARD 17)
add_compile_options(-Wall -Wextra -Werror -Wfatal-errors)

add_executable(smart-pointers
        instance-count.hxx
        account.hxx
        shared-ptr-app.cxx
        )
