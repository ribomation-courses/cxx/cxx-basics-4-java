
#include <memory>
#include "account.hxx"

int instance::count = 0;

shared_ptr<Account> func2(shared_ptr<Account> q) {
    cout << "[func2] q: " << *q << ", refcnt=" << q.use_count() << endl;
    *q += 200;
    return q;
}

shared_ptr<Account> func1(shared_ptr<Account> p) {
    cout << "[func1] p: " << *p << ", refcnt=" << p.use_count() << endl;
    *p += 100;
    return func2(p);
}

shared_ptr<Account> conversion(shared_ptr<Account> account) {
    string accno   = *account;
    int    balance = *account;
    cout << "[conv] accno=" << accno << ", balance=" << balance << endl;
    return account;
}

shared_ptr<Account> update(shared_ptr<Account> account) {
    cout << "[update] before: " << *account << endl;
    *account += 300;
    cout << "[update] after : " << *account << endl;
    return account;
}


int main() {
    cout << "# accounts: " << Account::numObjs() << endl;
    {
        auto ptr = make_shared<Account>("2222-12345", 100);
        ptr = update(ptr);
        {
            auto ptr2 = ptr;
            cout << "ptr2: " << *ptr2 << ", refcnt=" << ptr2.use_count() << endl;
            ptr2 = func1(ptr2);
            cout << "ptr2: " << *ptr2 << ", refcnt=" << ptr2.use_count() << endl;
        }
        cout << "ptr: " << *ptr << ", refcnt=" << ptr.use_count() << endl;
        ptr = conversion(ptr);
        cout << "ptr: " << *ptr << ", refcnt=" << ptr.use_count() << endl;
    }
    cout << "# accounts: " << Account::numObjs() << endl;
    return 0;
}

