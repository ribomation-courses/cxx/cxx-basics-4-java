#include <iostream>
#include <map>
#include <random>
#include <string>
#include <functional>
#include <algorithm>
#include <iomanip>

using namespace std;

void repeat(int n, const function<void()>& f) {
    for (auto k = 0; k < n; ++k) { f(); }
}

int main(int numArgs, char* args[]) {
    int mean = 0, stddev = 3, N = 1'000'000, W = 100;

    for (int k = 1; k < numArgs; ++k) {
        string arg = args[k];
        if (arg == "-mean") {
            mean = stoi(args[++k]);
        } else if (arg == "-dev") {
            stddev = stoi(args[++k]);
        } else if (arg == "-num") {
            N = stoi(args[++k]);
        } else if (arg == "-width") {
            W = stoi(args[++k]);
        } else {
            if (arg != "-?") cerr << "unknown option: " << arg << endl;
            cerr << "usage: " << args[0] << "[-mean <int>] [-dev <int>] [-num <int>] [-width <int>]" << endl;
            return 1;
        }
    }

    //default_random_engine      r;
    random_device       r;
    normal_distribution<float> gauss(mean, stddev);

    map<int, unsigned> histogram;
    repeat(N, [&] { histogram[gauss(r)]++; });

    using Bucket = pair<const int, unsigned>;
    auto maxFreq = max_element(histogram.begin(), histogram.end(), [](Bucket& left, Bucket& right) {
        return left.second < right.second;
    });
    const double scale = maxFreq->second / W;

    for (auto [value, freq] : histogram) {
        auto numStars = static_cast<unsigned long>(freq / scale);
        string stars(numStars, '*');
        cout << setw(3) << value << " " << stars << endl;
    }

    return 0;
}
