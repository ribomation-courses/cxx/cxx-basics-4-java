#pragma once

#include <chrono>
#include <tuple>
#include <functional>

using namespace std;
using namespace std::chrono;

template<typename Argument, typename Result>
tuple<Result, unsigned long>
elapsed(function<Result(Argument)> func, Argument arg) {
    auto start = high_resolution_clock::now();
    Result result = func(arg);
    auto stop  = high_resolution_clock::now();
    auto time = duration_cast<nanoseconds>(stop - start).count();
    return make_tuple(result, time);
}

